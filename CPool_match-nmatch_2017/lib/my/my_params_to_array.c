/*
** EPITECH PROJECT, 2017
** Day09
** File description:
** structure
*/

#include "../../include/my.h"
#include <stdlib.h>

struct	info_param	*my_params_to_array(int ac, char **av)
{
	struct	info_param	*test;
	int	i = 0;
	int	j = 0;

	test = malloc(sizeof(struct info_param) * ac);
	while (j < ac){
		test[j].length = my_strlen(av[i]);
		test[j].str = &av[i];
		test[j].copy = my_strdup(av[i]);
		test[j].word_array = my_str_to_word_array(av[i]);
		i++;
		j++;
	}
	return (test);
}
