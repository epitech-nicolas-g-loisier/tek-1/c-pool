/*
** EPITECH PROJECT, 2017
** projet 2 
** File description:
** match
*/

int	verif(char const *s1, int i, char const *s2, int j)
{
	printf("verif\n");
	j++;
	while (s1[i] != s2[j] || s1[i] == '\0')
		i++;
	return(i);
	
}
int	match(char const *s1, char const *s2)
{
	int	i = 0;
	int	j = 0;
	int	test;

	while (s1[i] != '\0' || s2[j] != '\0'){
		while (s1[i] == s2[j]){
			i++;
			j++;
		}
		if (s2[j] == '*'){
			i = verif(s1, i, s2, j);
			j++;
		}
		i++;
		j++;
	}
	if (s1[i - 1] == s2[j - 1])
		return (1);
	return (0);
}
