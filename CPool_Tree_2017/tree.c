/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** tree.c
*/

void my_putchar(char c);

int     calc_last(int size)
{
        int	star = 1;
	int	branch_line = 4;
	int	count_line = 0;
	int	less_star = 0;

	while	(branch_line <= size + 3){
		count_line = 0;
		while (count_line < branch_line){
			count_line++;
			star = star + 2;
		}
		branch_line++;
		star = star - 2;
		if (branch_line / 2 * 2 != branch_line && branch_line > 5)
			less_star = less_star + 2;
		star = star - less_star;
	}
	if (branch_line % 2 == 0)
		less_star++;
	return(star - less_star);
}

void	print_log(int size)
{
	int	count_line = 0;
	int	count_column = 0;
	int	column = size;
	int	space;

	if(size % 2 == 0)
		column++;
	while (count_line < size){
		space = (calc_last(size) - column) / 2;
		while (space > 0){
			my_putchar(32);
			space--;
		}
		while (count_column < column){
			my_putchar('|');
			count_column++;
		}
		my_putchar(10);
		count_column = 0;
		count_line++;
	}
}

void	print_branch(int size)
{
	int	star = 1;
	int	branch_line = 4;
	int	count_line;
	int	print_star = 0;
	int	space;
	int	less_star = 4;

	while (branch_line <= size + 3){
		count_line = 0;
		while (count_line < branch_line){
			print_star = print_star +  star;
			space = ((calc_last(size) - print_star) / 2);
			while ( space > 0){
				my_putchar(' ');
				space--;
			}
			while (print_star > 0){
				my_putchar('*');
				print_star--;
			}
			my_putchar('\n');
			count_line++;
			star = star + 2;
			       }
		branch_line++;
		if (branch_line / 2 * 2 != branch_line && branch_line > 5)
			less_star = less_star + 2;
		star = star - less_star;
	}
}

int	tree(int size)
{
	if (size <= 0)
		return(0);
	print_branch(size);
	print_log(size);
	return(0);
}
