#!/bin/sh
export MY_LINE1=$1
export MY_LINE2=$2
taille=$(($MY_LINE2 - $MY_LINE1+1))
echo $taille
cut -d: -f 1 | rev | sed -n 0~2p | sort -r | head -n $MY_LINE2 | sed -n $MY_LINE1~1p | tr '\n' '\ ' | sed -e 's/\ /\, /g' | cut -d, -f 1-$taille | tr '\n' '.' | cut -d, -f 1-$taille 
