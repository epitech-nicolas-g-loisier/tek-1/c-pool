/*
** EPITECH PROJECT, 2017
** emac
** File description:
** my_evil_str.c
*/
void my_putchar(char c);

int     my_strlenb(char const *str)
{
	int     i;

	i = 0;
	while(str[i] != 0)
		i = i + 1;
	return(i);
}

char	*my_evil_str(char *str)
{
	int	a;
	int b;
	int nbr = my_strlenb(str);
	char strb[str];

	while(a < nbr){
		strb[a] = str[a];
		a = a + 1;
	}
	nbr = nbr - 1;
	while(nbr >= 0){
		str[b] = strb[nbr];
		b = b + 1;
	}
	return(str);
}
