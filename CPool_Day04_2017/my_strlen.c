/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_strlen.c
*/

void my_putchar(char c);

int	my_strlen(char const *str)
{
	int	i;

	i = 0;
	while(str[i] != 0)
		i = i + 1;
	return(i);
}
