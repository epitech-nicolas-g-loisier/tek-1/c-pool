/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_compute_power_it.c
*/

int	my_compute_power_it(int nb, int p)
{
	long	pow;

	pow = nb;
	while(p > 1)
	{
		pow = pow * nb;
		p = p - 1;
	}
	if (pow > 2147483647 || p < 0)
		return (0);
	else if (p == 0)
		return(1);
	else
		return(pow);
}
