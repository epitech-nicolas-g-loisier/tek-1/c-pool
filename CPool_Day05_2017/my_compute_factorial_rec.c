/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_compute_factorial_rec.c
*/

int	my_compute_factorial_rec(int nb)
{
	int	fact = nb;
	int	nbr = nb;
	
	if(nb > 12 || nb < 0)
		return(0);
	else if(nb <= 1)
		return(1);
	else{
		fact = fact  * my_compute_factorial_rec(nbr - 1);
		return(fact);
	}
}
