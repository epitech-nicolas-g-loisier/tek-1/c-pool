/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_compute_factorial_it.c
*/

int	my_compute_factorial_it(int nb)
{
	int	fact  = nb;

	if(nb > 12 || nb < 0)
		return(0);
	else if(nb == 0)
		return(1);
	else{
		nb = fact - 1;
		while(nb > 0){
			fact = fact * nb;
			nb = nb - 1;
		}
		return(fact);
	}
}
