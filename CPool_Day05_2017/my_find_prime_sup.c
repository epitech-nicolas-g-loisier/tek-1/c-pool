/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_find_prime_sup.c
*/

int     my_find_prime_sup(int nb)
{
        int	div = 2;
	int	prime = 3;

	if(nb <= 2)
		return (2);
	while(prime < nb){
		prime = prime + 2;
	}
	while(prime <= 2147483647){
		while(prime % div && div != prime){
			div = div + 1;
			if(prime % div == 0 && div == prime)
				return(prime);
		}
		prime = prime + 2;
	}
	return(0);
}
