/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_is_prime.c
*/

int	my_is_prime(int nb)
{
	int	div = 2;
	int	tes;
	int	result = 0;

	tes = nb % 2;
       	if(nb == 2)
		result = 1;
	else if(tes == 0 || nb < 0)
		result = 0;
	else{
		while(div <= nb){
			div = div + 1;
			if(nb % div == 0 && div == nb)
				result = 1;
			else if(nb % div == 0)
				return(0);
		}
	}
	return(result);
}
