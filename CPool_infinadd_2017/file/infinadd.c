/*
** EPITECH PROJECT, 2017
** infinadd
** File description:
** 
*/

#include <stdlib.h>
#include <unistd.h>
#include "../include/my.h"

void	print(char *result)
{
	int	i = 0;
	int	test = 0;

	while ((result[i] <= 48 || result[i] >= 58) && result[i] != '\0')
		i++;
	while (result[i] != '\0'){
		my_putchar(result[i]);
		i++;
		test++;
	}
	if (test == 0)
		my_putchar('0');
	my_putchar('\n');
}

char	*malloc_res(int i, int j)
{
	char	*res;

	if (i < j)
		res = malloc(sizeof(char*) * (j + 1));
	else
		res = malloc(sizeof(char*) * (i + 1));
	return (res);
}

void	send(char *nb1, char *nb2)
{
	if (my_str_iszero(nb1) == 1 && my_str_iszero(nb2) == 1)
		my_putstr("0\n");
	else {
		if (nb1[0] == '-' && nb2[0] == '-'){
			my_putchar('-');
			add_cond(nb1, nb2);
		}
		else if (nb1[0] != '-' && nb2[0] != '-')
			add_cond(nb1, nb2);
		else
			subs_cond(nb1, nb2);
	}
}
int	main(int argc, char **argv)
{
	char	*nb1 = argv[1];
	char	*nb2 = argv[2];
	
	if (argc != 3){
		write(2, "invalid number of argument\n", 28);
		return (84);
	}
	else if (my_str_isnum(nb1) == 0 || my_str_isnum(nb2) == 0){
		write(2, "invalid argument\n", 18);
		return(84);
	}
	else
		send(nb1, nb2);
	return (0);
}
