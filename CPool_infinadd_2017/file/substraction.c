/*
** EPITECH PROJECT, 2017
** infinadd
** File description:
** substraction file
*/

#include <stdio.h>
#include <stdlib.h>
#include "../include/my.h"

int	substraction(int nbr1, int nbr2, int ret, int j)
{
	int     res = 0;

	nbr1 = nbr1 - 48;
	nbr2 = nbr2 - 48;
	if (j < 1)
		res = -(nbr1 - ret + 10);
	else if (nbr2 <= 0)
		res = -(nbr1 - ret + 10) ;
	else {
		if (nbr1 < nbr2){
			res = -((nbr1 + 10) - nbr2 - ret);
			if (res == 0)
				res = -10;
		}
		else if (nbr1 > 0 && nbr2 > 0 && nbr1 <= 9 && nbr2 <= 9)
			res = nbr1 - nbr2 - ret;
	}
	if (nbr1 < 0)
		res = 0;
	return (res);
}

void	subs(char *nbr1, char *nbr2, int i, int j)
{
	char	*res = malloc(sizeof(char *) * (i));
	int	ret = 0;
	int	a = i;
	int	result = 0;

	while (nbr1[i] != '-' && nbr1[i - 1] != '\0'){
		result = substraction(nbr1[i - 1], nbr2[j - 1], ret, j);
		i--;
		j--;
		ret = 0;
		if (result < 0){
			res[a] = ((-result) % 10) + 48;
			ret = 1;
		}
		else
			res[a] = result + 48;
		a--;
	}
	res[a] = '0';
	print(res);
}

void	subs_neg(char *nbr1, char *nbr2, int i, int j)
{
	char	*res = malloc(sizeof(char *) * (i));
	int	ret = 0;
	int	a = i;
	int	result = 0;

	while (nbr1[i] != '-' && nbr1[i - 1] != '\0'){
		result = substraction(nbr1[i - 1], nbr2[j - 1], ret, j);
		i--;
		j--;
		ret = 0;
		if (result < 0){
			res[a] = ((-result) % 10) + 48;
			ret = 1;
		}
		else
			res[a] = result + 48;
		a--;
	}
	res[a] = '0';
	if (my_str_iszero(res) == 0)
		my_putchar('-');
	print(res);
}

void	subs_cond(char *nb1, char *nb2)
{
	int	i = my_strlen(nb1);
	int	j = my_strlen(nb2);

	if (nb1[0] == '-'){
		if ((i - 1) > j)
			subs_neg(nb1, nb2, i, j);
		else if ((i - 1) == j && my_strcmp(nb1, nb2) > 0)
			subs_neg(nb1, nb2, i, j);
		else
			subs(nb2, nb1, j, i);
	}
	else if (nb2[0] == '-'){
		if ((j - 1) > i)
			subs_neg(nb2, nb1, j, i);
		else if ((j - 1) == i && my_strcmp(nb2, nb1) > 0)
			subs_neg(nb2, nb1, j, i);
		else
			subs(nb1, nb2, i, j);
	}
}
