/*
** EPITECH PROJECT, 2017
** infinadd
** File description:
** addition file
*/

#include "../include/my.h"

int	addition(int nb1, int nb2, int ret, int j)
{
	int	res;
	
	nb1 = nb1 - 48;
	nb2 = nb2 - 48;
	if ((j <= 0 && nb1 != -3) || nb2 == -3){
		res = nb1 + ret;
	}
	else if (nb1 >= 0 && nb2 >= 0 && nb1 <= 9 && nb2 <= 9)
		res = nb1 + nb2 + ret;
	else
		res = 0 + ret;
	return (res);
}

void	add(char *nb1, char *nb2, int i, int j)
{
	char	*res = malloc_res(i, j);
	int	a;
	int	ret = 0;
	int	result;

	if (j < i)
		a = i;
	else
		a = j;
	while (i > 0 || j > 0){
		result = addition(nb1[i - 1], nb2[j - 1], ret, j);
		i--;
		j--;
		ret = 0;
		if (result >= 10){
			res[a] = ((result % 10) + 48);
			ret = 1;
		}
		else
			res[a] = result + 48;
		a--;
	}
	res[a] = ret + 48;
	print(res);
}

void	add_cond(char *nb1, char *nb2)
{
	int     i = my_strlen(nb1);
	int     j = my_strlen(nb2);

	if (i > j){
		add(nb1, nb2, i, j);
	}
	else if (i == j && my_strcmp(nb1, nb2) >= 0){
		add(nb1, nb2, i, j);
	}
	else
		add(nb2, nb1, j, i);
}
