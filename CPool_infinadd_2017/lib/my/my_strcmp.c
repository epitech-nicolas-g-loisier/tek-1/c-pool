/*
** EPITECH PROJECT, 2017
** my_strncmp
** File description:
** strcmp
*/

int	my_strcmp(char const *s1, char const *s2)
{
	int	i = 0;

	while (s1[i + 1] == s2[i]){
		if (s1[i + 1] == '\0')
			return (0);
		i++;
	}
	if (s1[i + 1] < s2[i])
		return (-1);
	else
		return (1);
}
