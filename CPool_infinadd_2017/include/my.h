/*
** EPITECH PROJECT, 2017
** infinadd
** File description:
** prototypes
*/

void    print(char *result);
void	my_putchar(char c);
int	my_putstr(char const *str);
int	my_strcmp(char const *s1, char const *s2);
int	my_strlen(char const *str);
void    subs_cond(char *nb1, char *nb2);
char    *malloc_res(int i, int j);
void    add_cond(char *nb1, char *nb2);
int     my_str_isnum(char const *str);
int     my_str_iszero(char const *str);
