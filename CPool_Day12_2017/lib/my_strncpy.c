/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_strcpy.c
*/

char	*my_strncpy(char *dest, char const *src, int n)
{
	int	j = 0;

	while(j < n && dest[j] != '\0'){
		dest[j] = src[j];
		j = j + 1;
	}
	dest[j] = '\0';
	return(dest);
}
