/*
** EPITECH PROJECT, 2017
** Day08
** File description:
** str_to_word_array
*/

#include <stdio.h>
#include <stdlib.h>

int	verif_word(char str)
{
	if (str > 64 && str < 91)
		return(1);
	else if (str > 96 && str < 123)
		return(1);
	if (str > 47 && str < 58)
		return(1);
	return(0);
}

int	nbr_word(char *str)
{
	int	i = 0;
	int	word = 0;

	while (str[i] != '\0'){
		while (verif_word(str[i]) == 1)
			i++;
		if (verif_word(str[i - 1]) == 1){
			word++;
			i++;
		}
		else
			i++;
	}
	return(word);
}

char	**my_str_to_word_array(char const *str)
{
	int	i = 0;
	int	j = 0;
	int	count = 0;
	int	nbr_w;
	char	**dest;
	int	test;

	nbr_w = nbr_word(str);
	dest = malloc(sizeof(char*) * (nbr_w + 1));
	while (i < nbr_w){
		test = verif_word(str[count]);
		if (test == 1){
			dest[i] = &str[count];
			i++;
			while (test == 1){
				test = verif_word(str[count]);
				count++;
			}
		}
		else
			count++;
	}
	dest[i] = NULL;
	return (dest);
}
