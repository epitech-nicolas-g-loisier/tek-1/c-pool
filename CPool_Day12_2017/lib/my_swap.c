/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_swap.c
*/

void	my_swap(int *a, int *b)
{
	int	c;

	c = *a;
	*a = *b;
	*b = c;
}
