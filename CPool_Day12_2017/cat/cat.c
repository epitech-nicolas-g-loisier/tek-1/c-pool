/*
** EPITECH PROJECT, 2017
** Day12 task01
** File description:
** cat
*/

#include "../include/my.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>

void	my_puterror(char *message);

void	error(int i, char *name)
{
	my_puterror("cat: ");
	my_puterror(name);
	my_puterror(": ");
	if (i == 1)
		my_puterror("No such file or directory");
	else if (i == 2)
		my_puterror("Is a directory");
	my_putchar('\n');
	i = 0;
}

int	display(char **av, int i)
{
	int	fd = 0;
	char	buffer[256000];
	int	size = 1;
	int	e = 0;
	
	fd = open(av[i], O_RDONLY);
	if (fd == -1){
		error(1, av[i]);
		e = 1;
	}
	else if (fd >= 3){
		error(2, av[i]);
		e = 1;
	}
	size = read(fd, buffer, 2);
	while (size > 0){
		my_putstr(buffer);
		size = read(fd, buffer, 2);
	}
	fd = 0;
	close(av[i]);
	return (e);
}

void	without_param(void)
{
	char	str[200];
	int	i = 0;
	
	read(0, str, 200);
	my_putstr(str);
	while (str[i] != '\0'){
		str[i] = '\0';
		i++;
	}
}

int	main(int ac, char **av)
{

	int	i = 1;
	int	e = 0;

	if (ac == 1){
		while(0 == 0)
			without_param();
	}
	else{
		while (av[i] != NULL){
			e = e + display(av, i);
			i++;
		}
	}
	if (e == 1)
		return(84);
	return (0);
}
