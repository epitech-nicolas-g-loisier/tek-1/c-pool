/*
** EPITECH PROJECT, 2017
** 
** File description:
** error_message
*/

void	my_error(char c)
{
	write(2, &c, 1);
}

void	my_puterror(char *message)
{
	int	i = 0;

	while (message[i] != '\0'){
		my_error(message[i]);
		i++;
	}
}
