/*
** EPITECH PROJECT, 2017
** Day11
** File description:
** structure for Day11
*/

typedef	struct	linked_list
{
	void	*data;
	struct	linked_list	*next;
} linked_list_t;
