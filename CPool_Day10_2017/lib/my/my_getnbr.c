/*
** EPITECH PROJECT, 2017
** Day04 task05
** File description:
** my_getnbr
*/

int	check_nbr(char c)
{
	int	i;
	
	if (c > 47 && c < 58){
		i = c - 48;
		return (i);
	}
	else if (c == 45)
		return (-1);
	else
		return (0);
}

int	my_getnbr(char const *str)
{
	int	i = 0;
	int	test = 0;
	long	nbr = 0;
	int	neg = 1;
	
	while (i >= 0){
		test = check_nbr(str[i]);
		if (test > 0){
			nbr = (nbr * 10) + test;
			if (check_nbr(str[i - 1]) < 0 && nbr < 10)
				neg = -1;
			i++;
		}
		else if (test <= 0 && nbr > 0){
			if (nbr > 2147483647)
				return (0);
			nbr = neg * nbr;
			return (nbr);
		}
		else
			i++;
	}
}
