/*
** EPITECH PROJECT, 2017
** my_strdup.c
** File description:
** Day08
*/

#include <stdlib.h>

char	*my_strdup(char const *src)
{
	char	*dest;
	int	i = 0;

	while (src[i] != '\0')
		i++;
	dest = malloc(sizeof(char) * (i + 1));
	i = 0;
	while (src[i] != '\0'){
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return(dest);
}
