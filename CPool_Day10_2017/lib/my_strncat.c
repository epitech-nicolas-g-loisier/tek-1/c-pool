/*
** EPITECH PROJECT, 2017
** my_strn_cat
** File description:
** strn_cat
*/

char	*my_strncat(char *dest, char *src, int n)
{
	int	i = 0;
	int	j = 0;

	while (dest[i] != '\0'){
		i++;
	}
	while (j <= n){
		dest[i] = src[j];
		i++;
		j++;
	}
	return (dest);
}
