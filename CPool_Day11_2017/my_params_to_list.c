/*
** EPITECH PROJECT, 2017
** Day11 task01
** File description:
** my_params_to_list
*/

#include "include/my.h"
#include "include/mylist.h"
#include <stdlib.h>

int	my_put_in_list(linked_list_t **list, char *av)
{
	linked_list_t	*element;

	element = malloc(sizeof(*element));
	element->data = av;
	element->next = *list;
	*list = element;
	return (0);
}

linked_list_t	*my_params_to_list(int ac, char * const *av)
{
	linked_list_t	*list;
	int	i = 0;

	list = NULL;
	while (i < ac){
		my_put_in_list(&list, av[i]);
		i++;
	}
	return (list);
}
