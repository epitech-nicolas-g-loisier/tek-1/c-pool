/*
** EPITECH PROJECT, 2017
** Day11 task02
** File description:
** my_list_size
*/

int	my_list_size(linked_list_t const *begin)
{
	int	i = 0;

	while (begin[i] != NULL)
		i++;
	i--;
	return(i);
}
