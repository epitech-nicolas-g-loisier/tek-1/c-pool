/*
** EPITECH PROJECT, 2017
** my_str_isalpha.c
** File description:
** str_isalpha
*/

int	my_str_isprintable(char const *str)
{
	int	i = 0;

	while (str[i] != '\0'){
		if (str[i] > 32 && str[i] < 128)
			i++;
		else
			return (0);
	}
	return (1);
}
