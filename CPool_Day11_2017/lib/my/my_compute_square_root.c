/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_compute_square_root.c
*/

int	my_compute_square_root(int nb)
{
	int	root = 0;
	int	test = 0;

	if (nb < 0)
		return(0);
	while (test < nb){
		root = root + 1;
		test = root * root;
	}
	if (test != nb)
		return(0);
	return(root);
}
