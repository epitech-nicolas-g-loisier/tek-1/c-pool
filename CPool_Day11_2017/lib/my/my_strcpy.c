/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_strcpy.c
*/

char	*my_strcpy(char *dest, char const *src)
{
	int	i = 0;

	while(src[i] != '\0'){
		dest[i] = src[i];
		i = i + 1;
	}
	dest[i] = src[i];
	return(0);
}
