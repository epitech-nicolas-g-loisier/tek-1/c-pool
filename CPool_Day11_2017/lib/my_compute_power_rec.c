/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_compute_power_rec.c
*/

int	my_compute_power_rec(int nb, int p)
{
	long	pow;

	pow = nb;
	if(p < 0 || pow > 2147483647)
		return(0);
	else if (p == 0)
		return(1);
	else{
		pow = pow * my_compute_power_rec(pow, p - 1);
		return(pow);
	}
}
