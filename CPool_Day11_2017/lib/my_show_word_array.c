/*
** EPITECH PROJECT, 2017
** Day08
** File description:
** show_word_array
*/

int	my_putstr(char const *str);

int	my_show_word_array(char * const *tab)
{
	int	i = 0;
	
	while (tab[i] != '\0'){
		my_putstr(tab[i]);
		i++;
	}
	return (0);
}
