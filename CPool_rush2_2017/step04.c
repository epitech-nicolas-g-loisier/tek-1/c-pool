/*
** EPITECH PROJECT, 2017
** rush2
** File description:
** step4
*/

#include "include/my.h"

int	English(int proba, char letter);
int	French(int proba, char letter);
int	German(int proba, char letter);
int	Spanish(int proba, char letter);

int	absolute_value(int nbr)
{
	if (nbr < 0)
		nbr = -nbr;
	return (nbr);
}

int	step04(int proba, char *sentence)
{
	int	eng = 0;
	int	fr = 0;
	int	ger = 0;
	int	sp = 0;
	int	i = 0;

	my_putchar(61);
	my_putchar(62);
	my_putchar(32);
	while (sentence[i] != '\0'){
		eng = eng + absolute_value(English(proba, sentence[i]));
		fr = fr + absolute_value(French(proba, sentence[i]));
		ger = ger + absolute_value(German(proba, sentence[i]));
		sp = sp + absolute_value(Spanish(proba, sentence[i]));
		i++;
	}
	if (eng <= fr && eng <= ger && eng <= sp)
		my_putstr("English");
	else if (fr <= eng && fr <= ger && fr <= sp)
		my_putstr("French");
	else{
		if (ger <= eng && ger <= fr && ger <= sp)
			my_putstr("Spanish");
		else
			my_putstr("German");
	}
	return (0);
}
