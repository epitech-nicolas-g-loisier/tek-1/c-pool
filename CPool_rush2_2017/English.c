/*
** EPITECH PROJECT, 2017
** rush2
** File description:
** english
*/

int	E_a_f(int proba, char letter)
{
	int	a = 816;
	int	b = 149;
	int	c = 278;
	int	d = 425;
	int	e = 1270;
	int	f = 222;

	if (letter == 'a'){
		return (a - proba);
	}
	else if (letter == 'b')
		return (b - proba);
	else{
		if (letter == 'c')
			return (c - proba);
		else if (letter == 'd')
			return (d - proba);
	}
	if (letter == 'e')
		return (e - proba);
	else if (letter == 'f')
		return (f - proba);
}

int	E_g_l(int proba, char letter)
{
	int	g = 201;
	int	h = 609;
	int	i = 696;
	int	j = 15;
	int	k = 77;
	int	l = 402;

	if (letter == 'g')
		return (g - proba);
	else if (letter == 'h')
		return (h - proba);
	else {
		if (letter == 'i')
			return (i - proba);
		else if (letter == 'j')
			return (j - proba);
	}
	if (letter == 'k')
		return (k - proba);
	else if (letter == 'l')
		return (l - proba);
}

int	E_m_s(int proba, char letter)
{
	int	m = 240;
	int	n = 674;
	int	o = 750;
	int	p = 192;
	int	q = 9;
	int	r = 598;
	int	s = 632;

	if (letter == 'm')
		return (m - proba);
	else if (letter == 'n')
		return (n - proba);
	else {
		if (letter == 'o')
			return (o - proba);
		else if (letter == 'p')
			return (p - proba);
	}
	if (letter == 'q')
		return (q - proba);
	else if (letter == 'r')
		return (r - proba);
	else
		return (s - proba);
}

int	E_t_z(int proba, char letter)
{
	int	t = 905;
	int	u = 275;
	int	v = 97;
	int	w = 236;
	int	x = 15;
	int	y = 197;
	int	z = 7;

	if (letter == 't')
		return (t - proba);
	else if (letter == 'u')
		return (u - proba);
	else {
		if (letter == 'v')
			return (v - proba);
		else if (letter == 'w')
			return (w - proba);
	}
	if (letter == 'x')
		return (x - proba);
	else if (letter == 'y')
		return (y - proba);
	else
		return (z - proba);
}

int	English(int proba, char letter)
{
	int	count = 0;

	if (letter <= 102 && letter >= 97)
		count = E_a_f(proba, letter);
	else if (letter <= 108 && letter >= 103)
		count = E_g_l(proba, letter);
	if (letter <= 115 && letter >= 109)
		count = E_m_s(proba, letter);
	else if (letter <= 122 && letter >= 116)
		count = E_t_z(proba, letter);
	return (count);
}
