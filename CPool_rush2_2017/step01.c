/*
** EPITECH PROJECT, 2017
** rush2
** File description:
** step1
*/

#include "include/my.h"
#include <stdlib.h>
#include <unistd.h>

char	my_put_error(char c);
char	*error_message(void);

char	test_letter(char l)
{
	if (l < 123 && l > 96){
		return (l);
	}
	else if (l < 91 && l > 64){
		l = l + 32;
		return (l);
	}
	else{
		l = 35;
		return (l);
	}
}

int	count_letter(char *tab, char l)
{
	int	i = 0;
	int	count = 0;

	while (tab[i] != '\0'){
		if (l == tab[i])
			count++;
		i++;
	}
	return (count);
}

void	display(char letter, int nbr_l)
{
	my_putchar(letter);
	my_putchar(58);
	my_put_nbr(nbr_l);
}

int	step01(char *sentence, char letter)
{
	int	i = 0;
	int	nbr_l;
	char	l;

	sentence = my_strlowcase(sentence);
	l = test_letter(letter);
	if (l == 35){
		error_message();
		return (0);
	}
	nbr_l = count_letter(sentence, l);
	display(letter, nbr_l);
	return (nbr_l);
}
