/*
** EPITECH PROJECT, 2017
** Spanish.c
** File description:
** rush
*/


int	S_a_f(int proba, char letter)
{
	int	a = 1152;
	int	b = 221;
	int	c = 401;
	int	d = 501;
	int	e = 1218;
	int	f = 69;

	if (letter == 'a')
		return (a - proba);
	else if (letter == 'b')
		return (b - proba);
	else {
		if (letter == 'c')
			return (c - proba);
		else if (letter == 'd')
			return (d - proba);
	}
	if (letter == 'e')
		return (e - proba);
	else if (letter == 'f')
		return (f - proba);
}

int	S_g_l(int proba, char letter)
{
	int	g = 176;
	int	h = 70;
	int	i = 624;
	int	j = 49;
	int	k = 1;
	int	l = 496;

	if (letter == 'g')
		return (g - proba);
	else if (letter == 'h')
		return (h - proba);
	else {
		if (letter == 'i')
			return (i - proba);
		else if (letter == 'j')
			return (j - proba);
	}
	if (letter == 'k')
		return (k - proba);
	else if (letter == 'l')
		return (l - proba);
}

int	S_m_s(int proba, char letter)
{
	int	m = 315;
	int	n = 671;
	int	o = 868;
	int	p = 251;
	int	q = 87;
	int	r = 687;
	int	s = 797;

	if (letter == 'm')
		return (m - proba);
	else if (letter == 'n')
		return (n - proba);
	else {
		if (letter == 'o')
			return (o - proba);
		else if (letter == 'p')
			return (p - proba);
	}
	if (letter == 'q')
		return (q - proba);
	else if (letter == 'r')
		return (r - proba);
	else
		return (s - proba);
}

int	S_t_z(int proba, char letter)
{
	int	t = 463;
	int	u = 292;
	int	v = 113;
	int	w = 1;
	int	x = 21;
	int	y = 100;
	int	z = 46;

	if (letter == 't')
		return (t - proba);
	else if (letter == 'u')
		return (u - proba);
	else {
		if (letter == 'v')
			return (v - proba);
		else if (letter == 'w')
			return (w - proba);
	}
	if (letter == 'x')
		return (x - proba);
	else if (letter == 'y')
		return (y - proba);
	else
		return (z - proba);
}

int	Spanish(int proba, char letter)
{
	int	count = 0;

	if (letter <= 102 && letter >= 97)
		count = S_a_f(proba, letter);
	else if (letter <= 108 && letter >= 103)
		count = S_g_l(proba, letter);
	if (letter <= 115 && letter >= 109)
		count = S_m_s(proba, letter);
	else if (letter <= 122 && letter >= 116)
		count = S_t_z(proba, letter);
	return (count);
}
