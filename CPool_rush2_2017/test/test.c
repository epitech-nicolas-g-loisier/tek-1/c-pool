/*
** EPITECH PROJECT, 2017
** unit test
** File description:
** test step 1
*/

#include <criterion/criterion.h>

Test(step01, first_test)
{
	cr_assert(rush("azertyuiopqsdfghjklmwxcvbn", "a") == "a = 1", "error test a clavier");
}
