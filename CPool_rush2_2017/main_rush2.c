/*
** EPITECH PROJECT, 2017
** rush2
** File description:
** main
*/

void	error_message(void);

int	step02(int argc, char **argv);

int	main(int argc, char **argv)
{
	int	lang;

	if (argc < 3){
		error_message();
		return (84);
	}
	lang = step02(argc,argv);
	return (0);
}
