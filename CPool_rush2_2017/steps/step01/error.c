/*
** EPITECH PROJECT, 2017
** rush2
** File description:
** error
*/

#include "my.h"
#include <unistd.h>

char	my_put_error(char c)
{
	write(2, &c, 1);
}

char	*error_message(void)
{
	char	*error = "error";
	int	i = 0;

	while (error[i] != '\0'){
		my_put_error(error[i]);
		i++;
	}
	my_putchar(10);
}
