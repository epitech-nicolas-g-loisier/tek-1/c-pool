/*
** EPITECH PROJECT, 2017
** rush2
** File description:
** step2 - 3
*/

void	my_print_nbr(int proba)
{
	int	entier = proba / 100;
	int	deci = proba % 100;

	my_put_nbr(entier);
	my_putchar(46);
	if (proba < 10){
		my_putchar(48);
		my_put_nbr(deci);
	}
	else
		my_put_nbr(deci);
}

int	length(char *test)
{
	int	i = 0;
	int	count = 0;

	while (test[i] != '\0'){
		if (test[i] != 32)
			count++;
		i++;
	}
	return (count);
}

int	step03(int nbr_l, int size)
{
	int	proba = 0;
	int	i = 0;

	proba = (nbr_l * 10000) / size;
	my_putchar(32);
	my_putchar(40);
	my_print_nbr(proba);
	my_putchar(37);
	my_putchar(41);
	my_putchar(10);
	return(0);
}
int	step02(int argc, char **argv)
{
	int	i = 2;
	int	nbr_l = 0;
	int	size = length(argv[1]);

	while (i < argc){
		nbr_l = step01(argv[1], argv[i][0]);
		step03(nbr_l, size);
		i++;
	}
	return(0);
}
