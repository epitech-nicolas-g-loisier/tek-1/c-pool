/*
** EPITECH PROJECT, 2017
** French.c
** File description:
** rush
*/

int	F_a_f(int proba, char letter)
{
	int	a = 763;
	int	b = 90;
	int	c = 326;
	int	d = 366;
	int	e = 1471;
	int	f = 106;

	if (letter == 'a')
		return (a - proba);
	else if (letter == 'b')
		return (b - proba);
	else {
		if (letter == 'c')
			return (c - proba);
		else if (letter == 'd')
			return (d - proba);
	}
	if (letter == 'e')
		return (e - proba);
	else if (letter == 'f')
		return (f - proba);
}

int	F_g_l(int proba, char letter)
{
	int	g = 86;
	int	h = 73;
	int	i = 752;
	int	j = 61;
	int	k = 4;
	int	l = 545;

	if (letter == 'g')
		return (g - proba);
	else if (letter == 'h')
		return (h - proba);
	else {
		if (letter == 'i')
			return (i - proba);
		else if (letter == 'j')
			return (j - proba);
	}
	if (letter == 'k')
		return (k - proba);
	else if (letter == 'l')
		return (l - proba);
}

int	F_m_s(int proba, char letter)
{
	int	m = 296;
	int	n = 709;
	int	o = 579;
	int	p = 252;
	int	q = 136;
	int	r = 669;
	int	s = 794;

	if (letter == 'm')
		return (m - proba);
	else if (letter == 'n')
		return (n - proba);
	else {
		if (letter == 'o')
			return (o - proba);
		else if (letter == 'p')
			return (p - proba);
	}
	if (letter == 'q')
		return (q - proba);
	else if (letter == 'r')
		return (r - proba);
	else
		return (s - proba);
}

int	F_t_z(int proba, char letter)
{

	int	t = 724;
	int	u = 631;
	int	v = 183;
	int	w = 7;
	int	x = 42;
	int	y = 12;
	int	z = 32;

	if (letter == 't')
		return (t - proba);
	else if (letter == 'u')
		return (u - proba);
	else {
		if (letter == 'v')
			return (v - proba);
		else if (letter == 'w')
			return (w - proba);
	}
	if (letter == 'x')
		return (x - proba);
	else if (letter == 'y')
		return (y - proba);
	else
		return (z - proba);
}

int	French(int proba, char letter)
{
	int	count = 0;

	if (letter <= 102 && letter >= 97)
		count = F_a_f(proba, letter);
	else if (letter <= 108 && letter >= 103)
		count = F_g_l(proba, letter);
	if (letter <= 115 && letter >= 109)
		count = F_m_s(proba, letter);
	else if (letter <= 122 && letter >= 116)
		count = F_t_z(proba, letter);
	return (count);
}
