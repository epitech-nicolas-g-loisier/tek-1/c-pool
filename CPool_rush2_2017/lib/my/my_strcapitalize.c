/*
** EPITECH PROJECT, 2017
** my_strcapitalize
** File description:
** strcapitalize
*/

char    *lowcase(char *str)
{
        int     i = 0;

        while (str[i] != '\0'){
                if(str[i] > 64 && str[i] < 91){
                        str[i] = str[i] + 32;
                }
                i = i + 1;
        }
        return (str);
}

char	*reverse(char *str)
{
	int	i = 0;

	while (str[i] != '\0'){
		if (str[i] > 96 && str[i] < 123){
			str[i] = str[i] - 32;
			i++;
		}
		else if (str[i] > 64 && str[i] < 91){
			str[i] = str[i] + 32;
			i++;
		}
		else
			i++;
	}
	if (str[i - 1] != '.'){
		str[i] = '.';
		str[i + 1] = '\0';
	}
	return (str);
}

char	*my_strcapitalize(char *str)
{
	int	i = 0;
	int	letter = 0;
	int	num = 0;

	str = lowcase(str);
	while (str[i] != '\0'){
		if (str[i] > 96 && str[i] < 123){
			if (str[i - 1] > 96 && str[i - 1] < 123)
				letter++;
			else if (str[i - 1] > 64 && str[i - 1] < 91)
				letter++;
			if (str[i - 1] > 48 && str[i - 1] < 58)
				num++;
			if (num > 0 || letter > 0)
				str[i] = str[i] - 32;
			num = 0;
			letter = 0;
		}
		i++;
	}
	reverse(str);
	return (str);
}
