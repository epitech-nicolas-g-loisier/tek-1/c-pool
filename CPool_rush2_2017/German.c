/*
** EPITECH PROJECT, 2017
** German.c
** File description:
** rush
*/

int	G_a_f(int proba, char letter)
{
	int	a = 651;
	int	b = 188;
	int	c = 273;
	int	d = 507;
	int	e = 1639;
	int	f = 165;

	if (letter == 'a')
		return (a - proba);
	else if (letter == 'b')
		return (b - proba);
	else {
		if (letter == 'c')
			return (c - proba);
		else if (letter == 'd')
			return (d - proba);
	}
	if (letter == 'e')
		return (e - proba);
	else if (letter == 'f')
		return (f - proba);
}

int	G_g_l(int proba, char letter)
{
	int	g = 300;
	int	h = 457;
	int	i = 655;
	int	j = 26;
	int	k = 141;
	int	l = 343;

	if (letter == 'g')
		return (g - proba);
	else if (letter == 'h')
		return (h - proba);
	else {
		if (letter == 'i')
			return (i - proba);
		else if (letter == 'j')
			return (j - proba);
	}
	if (letter == 'k')
		return (k - proba);
	else if (letter == 'l')
		return (l - proba);
}

int	G_m_s(int proba, char letter)
{
	int	m = 253;
	int	n = 977;
	int	o = 259;
	int	p = 67;
	int	q = 1;
	int	r = 700;
	int	s = 727;

	if (letter == 'm')
		return (m - proba);
	else if (letter == 'n')
		return (n - proba);
	else {
		if (letter == 'o')
			return (o - proba);
		else if (letter == 'p')
			return (p - proba);
	}
	if (letter == 'q')
		return (q - proba);
	else if (letter == 'r')
		return (r - proba);
	else
		return (s - proba);
}

int	G_t_z(int proba, char letter)
{
	int	t = 615;
	int	u = 416;
	int	v = 84;
	int	w = 192;
	int	x = 3;
	int	y = 3;
	int	z = 113;

	if (letter == 't')
		return (t - proba);
	else if (letter == 'u')
		return (u - proba);
	else {
		if (letter == 'v')
			return (v - proba);
		else if (letter == 'w')
			return (w - proba);
	}
	if (letter == 'x')
		return (x - proba);
	else if (letter == 'y')
		return (y - proba);
	else
		return (z - proba);
}

int	German(int proba, char letter)
{
	int	count = 0;

	if (letter <= 102 && letter >= 97)
		count = G_a_f(proba, letter);
	else if (letter <= 108 && letter >= 103)
		count = G_g_l(proba, letter);
	if (letter <= 115 && letter >= 109)
		count = G_m_s(proba, letter);
	else if (letter <= 122 && letter >= 116)
		count = G_t_z(proba, letter);
	return (count);
}
