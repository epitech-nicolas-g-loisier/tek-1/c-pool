/*
** EPITECH PROJECT, 2017
** rush2
** File description:
** error
*/

#include "my.h"
#include <unistd.h>

char	my_put_error(char c)
{
	write(2, &c, 1);
}

char	*error_message(int type)
{
	char	*error = "error";
	int	i = 0;
	char	error_type;

	while (error[i] != '\0'){
		my_put_error(error[i]);
		i++;
	}
	if (type == 1)
		my_putstr(": Invalid number of argument");
	else if (type == 2)
		my_putstr(": Invalid number of character");
}
