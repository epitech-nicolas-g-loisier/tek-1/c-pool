/*
** EPITECH PROJECT, 2017
** concat_params.c
** File description:
** Day08
*/

#include <stdlib.h>

char	*str_size(int ac, char **av)
{
	int     i = 0;
        int     j = 0;
	int     count = 0;
        char    *str;
	
        while (i < ac){
                if (av[i][j] == '\0'){
                        i++;
                        j = 0;
                        count++;
                }
                else{
                        j++;
                        count++;
                }
        }
        str = malloc(sizeof(char) * (count + 1) + sizeof(char) * i);
	return(str);
}

char	*concat_params(int argc, char **argv)
{
	int	i = 0;
	int	j = 0;
	int	count = 0;
	char	*str;

	str = str_size(argc,argv);
	while (i < argc){
		str[count] = argv[i][j];
		if (argv[i][j] == '\0'){
			i++;
			str[count] = 10;
			count++;
			j = 0;
		}
		else{
			count++;
			j++;
		}
	}
	str[count - 1] = '\0';
	return(str);
}
