/*
** EPITECH PROJECT, 2017
** parenthesis
** File description:
** functions for check parenthesis
*/

int nbpar(char *str)
{
	int i = 0;
	int nbp = 0;
	while(str[i] != '\0'){
		if(str[i] == '('){
			nbp++;
		}
		i++;
	}
	return (nbp);
}

int posp_open(char *str, int nbp)
{
	nbp--;
	int i = 0;
	while(str[i] != '\0'){
		if(str[i] == '('){
			if(nbp == 0){
				return (i);
			}
			else
				nbp--;
		}
		i++;
	}
}

int posp_close(char *str, int nbp)
{
	int i = 0;
	int cp = 0;
	while(str[i] != '\0'){
		if(str[i] == ')'){
			if(cp == nbp){
				return (i);
			}
			else
				cp++;
		}
		i++;
	}
}

int check_par(char *str)
{
	int i = 0;
	while(str[i] != '\0'){
		if(str[i] == '('){
			return (1);
		}
		i++;
	}
	return (0);
}
	
