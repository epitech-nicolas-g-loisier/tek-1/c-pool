/*
** EPITECH PROJECT, 2017
** Bistr-matic
** File description:
** function to check char 
*/

int	check_is_neg(char c)
{
	if (c == '+' || c == '-' || c == '(')
		return (1);
	else if (c == '*' ||c == '/' || c == '%')
		return (1);
	else
		return (0);
}

int	check_op_is_prio(char c)
{
	if (c == '*' ||c == '/' || c == '%')
		return (1);
	else
		return (0);
}

int	check_op(char c)
{
	if (c == '+' ||c == '-')
		return (1);
	else if (c == '*' ||c == '/' || c == '%')
		return (1);
	else
		return (0);
}
