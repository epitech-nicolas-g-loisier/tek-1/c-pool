/*
** EPITECH PROJECT, 2017
** string_functions
** File description:
** funtcions that manage strings
*/

#include <stdlib.h>
/*
**This function check if there is zero before the number and delete them
*/
char *deletezero(char *str)
{
	int i = 0;

	if(str[i] == '0'){
		while(str[i] != '\0'){
			str[i] = str[i + 1];
			i++;
		}
	}
	str[i] = '\0';
	return (str);
}

/*
**This function adapt the lenght of a string to another with 0
**given in parameter
*/
char *adapt(char *str, int size_init, int size_final)
{
	char *newstr = malloc(sizeof(char) * size_final);
	while(size_final >= 0){
		if(isnum(str[size_init]) == 1){
			newstr[size_final] = str[size_init];
			size_init--;
			size_final--;
		}
		else{
			newstr[size_final] = '0';
			size_final--;
		}
	}
	return (newstr);
}

/*
**This funtion shift all characters of a string given in parameter
**from right to left for one hut of the string
*/ 
char *shift(char *str)
{
	int i = 0;
	while(str[i] != '\0'){
		str[i] = str[i + 1];
		i++;
	}
	str[i] = '\0';
	return (str);
}

/*
**This function add a less sign before a string given in parameter
*/
char *lessbefore(char *res, int lenght)
{
	char *newres = malloc(lenght + 1);
	int i = 1;
	newres[0] = '-';
	while(i < (lenght + 1)){
		newres[i] = res[i - 1];
		i++;
	}
	return (newres);
}
