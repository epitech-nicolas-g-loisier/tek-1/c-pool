/*
** EPITECH PROJECT, 2017
** char_functions
** File description:
** funcions that manage characters
*/

#include <stdlib.h>

/*
**This function get the value of a character given in parameter
**and return the int value (ascii) of this number
*/
int getint(char c)
{
	int res = 0;
	
	if(c >= 48 && c <= 57){
		res = c - 48;
	}
	return (res);
}

/*
**Check if the character given in parameter is a num or not
*/
int isnum(char c)
{
	if(c >= 48 && c <= 57)
		return (1);
	else
		return (0);
}
