/*
** EPITECH PROJECT, 2017
** infin_less
** File description:
** less an infinity of numbers
*/

#include "../include/my.h"
#include "../include/macro.h"
#include <stdio.h>

int isnegorpos(int biggest, int negatif)
{
	int signe;
	if(biggest == negatif){
		signe = 1;
	}
	else
		signe = 0;
	return (signe);
}

char *infin_less(char *nb1, char *nb2, char *res, int lenght, int biggest)
{
	int retenue = 0;
	int n1;
	int n2;
	if(biggest == 1){
		while(lenght > 0){
			n1 = getint(nb1[lenght - 1]);
			n2 = getint(nb2[lenght - 1]);
			if((n1 - n2 - retenue) < 0){
				res[lenght - 1] = n1 - n2 - retenue + 48 + 10;
				retenue = 1;
			}
			else{
				res[lenght - 1] = n1 - n2 - retenue + 48;
				retenue = 0;
			}
			lenght--;
		}
	}
	else if(biggest == 2){
		while(lenght > 0){
			n1 = getint(nb1[lenght - 1]);
			n2 = getint(nb2[lenght - 1]);
			if((n2 - n1 - retenue) < 0){
				res[lenght - 1] = n2 - n1 - retenue + 48 + 10;
				retenue = 1;
			}
			else{
				res[lenght - 1] = n2 - n1 - retenue + 48;
				retenue = 0;
			}
			lenght--;
		}
	}
	else{
		res[0] = '0';
		res[1] = '\0';
	}
	return (res);
}

int check_error(char *nb1, char *nb2)
{
	if(nb1[0] == '-' && nb1[1] == '0' || nb2[0] == '-' && nb2[1] == '0'){
		return (0);
	}
	else
		return (1);
	
}
