/*
** EPITECH PROJECT, 2017
** infin_add
** File description:
** add an infinity of numbers
*/

#include "../include/my.h"
#include "../include/macro.h"
#include <stdio.h>
#include <stdlib.h>

char *allocation(int lenght, int signe){
	char *res;
	if(signe == 0){
		res = malloc(lenght + 1);
	}
	else {
		res = malloc(lenght + 2);
	}
	return (res);
}

char *infin_add(char *nb1, char *nb2)
{
	char *res;
	int signe = 0;
	int len1 = my_strlen(nb1);
	int len2 = my_strlen(nb2);
	int lenght;
	int biggest = 0;
	if(check_error(nb1, nb2) == 0){
		res = malloc(sizeof(char) * 2);
		res[0] = '0';
		res[1] = '\0';
		return (res);
	}
	if(check_error(nb1, nb2) == 84)
		return (res);
	if(nb1[0] == '-' && nb2[0] == '-'){
		//--------------MOINS MOINS-----------------
		signe  = 1;
		//enlever les -
		nb1 = shift(nb1);
		nb2 = shift(nb2);
		len1--; len2--;
		//adapter
		if(len1 > len2){
			nb2 = adapt(nb2, (len2 - 1), (len1 - 1));
			lenght = len1;
		}
		else if(len1 < len2){
			nb1 = adapt(nb1, (len1 - 1), (len2 - 1));
			lenght = len2;
		}
		else
			lenght = len1;
		//allouer
		res = allocation(lenght, signe);
		//calculer
		res = compute_add(nb1, nb2, res, lenght);
		//remettre les moins devant le resultat
		res = lessbefore(res, lenght);
	}
	else if(nb1[0] == '-' || nb2[0] == '-'){
		//--------------PLUS MOINS------------------
		int negatif = 0;
		//suppression des moins et modification des tailles des nbr
		if(nb1[0] == '-'){
			nb1 = shift(nb1);
			len1--;
			negatif = 1;
		}
		else{
			nb2 = shift(nb2);
			len2--;
			negatif = 2;
		}
		//adapter les nbombres
		if(len1 > len2){
			nb2 = adapt(nb2, (len2 - 1), (len1 - 1));
			lenght = len1;
			biggest = 1;
		}
		else if(len1 < len2){
			nb1 = adapt(nb1, (len1 - 1), (len2 - 1));
			lenght = len2;
			biggest = 2;
		}
		else{
			//trouver le plus grand nombre si les tailles sont égales
			lenght = len1;
			if(my_strcmp(nb1, nb2) > 0){
				biggest = 1;
			}
			else if(my_strcmp(nb1, nb2) < 0){
				biggest = 2;
			}
			else{
				biggest = 0;
			}
		}
		//determiner si resultat est negatif
		signe = isnegorpos(biggest, negatif);
		//allocation de res
		res = allocation(lenght, signe);
		//appel de la fonction de soustraction
		res = infin_less(nb1, nb2, res, lenght, biggest);
		//enlever les zeros devant le res
		if(res[0] == '0')
			res = deletezero(res);
		//mettre le moins devant si resultat negatif
		if(signe == 1){
			res = lessbefore(res, lenght);
		}
	}
	else{
		//--------------PLUS PLUS-------------------
		//adapt
		if(len1 > len2){
			nb2 = adapt(nb2, (len2 - 1), (len1 - 1));
			lenght = len1;
			
		}
		else if(len1 < len2){
			nb1 = adapt(nb1, (len1 - 1), (len2 - 1));
			lenght = len2;
		}
		else{
			lenght = len1;
		}
		res = allocation(lenght, signe);
		res = compute_add(nb1, nb2, res, lenght);
	}
	return (res);
}

char *compute_add(char *nb1, char *nb2, char *res, int lenght)
{
	int i = 0;
	int retenue = 0;
	int n1;
	int n2;
	int savelen = lenght;
	while(lenght > 0){
		n1 = getint(nb1[lenght - 1]);
		n2 = getint(nb2[lenght - 1]);
		if(n1 + n2 + retenue > 9){
			res[lenght] = n1 + n2 + retenue - 10 + 48;
			retenue = 1;
		}
		else{
			res[lenght] = n1 + n2 + retenue + 48;
			retenue = 0;
		}
		lenght--;
	}
	if(retenue > 0){
		res[0] = retenue + 48;
	}
	else{
		while(i < savelen){
			res[i] = res[i + 1];
			i++;
		}
		res[i] = '\0';
	}
	return (res);
}
