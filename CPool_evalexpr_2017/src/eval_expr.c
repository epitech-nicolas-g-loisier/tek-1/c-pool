/*
** EPITECH PROJECT, 2017
** 
** File description:
** 
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "../include/my.h"

char	*clear_result(char*);
char	*calcul(char*);

char	*replace_par(char *src)
{
	int	str_len = my_strlen(src);
	int	i = 0;
	int	j = 1;
	int	tmp = 1;
	char	*dest = malloc(sizeof(char*) * (str_len + 1));

	if (src[1] == '-'){
		while (src[tmp] == '-'){
			dest[i] = '-';
			i++;
			j++;
			tmp++;
		}
		if (tmp % 2 == 1){
			i = 0;
			while (i != tmp-1){
				dest[i] = '0';
				i++;
			}
		}
	}
	dest[i] = '0';
	i++;
	dest[i] = '0';
	i++;
	while (i < str_len ){
		dest[i] = src[j];
		i++;
		j++;
	}
	dest[i] = 0;
	return (dest);
}

char	*find_calc_in_par(char *calc, int stop)
{
	int	i = 0;
	char	*calc_in_par = malloc(sizeof(char*) * (stop + 2));
	char	*calc_to_do = malloc(sizeof(char*) * (stop + 2));
	
	while (i <= stop){
		calc_in_par[i] = calc[i];
		i++;
	}
	calc_in_par[i] = 0;
	calc_to_do = replace_par(calc_in_par);
	printf("--------->calc_to_do: %s\n",calc_to_do);
	free(calc_in_par);
	return (calc_to_do);
}

char	*replace_calc(char *par, int stop, char *src)
{
	printf("--------->calc: %s\n",src);
	int	i = 0;
	int	begin = (my_strlen(src) - my_strlen(par));
	int	j = 0;
	char	*dest;
	char	*calc = calcul(find_calc_in_par(par, stop));
	
	dest = malloc(sizeof(char*) * (my_strlen(src) + 1));
	while (i < begin){
		dest[i] = src[i];
		i++;
	}
	while (j <= stop){
		dest[i] = calc[j];
		i++;
		j++;
	}
	while (src[i] != '\0'){
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return(dest);
}

char	*parenthesis(char *src)
{
	int	i = 0;
	int	j;

	while (src[i] != 0){
		if (src[i] == '(')
			j = i;
		else if (src[i] == ')'){
			return (replace_calc(&src[j], i - j, src));
		}
		i++;
	}
	return (src);
}

void	eval_expr(char *src)
{
	int	i = 0;
	
	while (src[i] != 0){
		if (src[i] == '('){
			src = parenthesis(src);
			i = -1;
		}
		i++;
	}
	src = calcul(src);
	my_putstr("res =  ");
	src = clear_result(src);
	my_putstr(src);
	//free(src);
	my_putchar('\n');
}

int	main(int ac, char **av)
{
	if (ac == 2){
		eval_expr(av[1]);
		return(0);
	}
	return(84);
}
