/*
** EPITECH PROJECT, 2017
** bistro-matic
** File description:
** clear number
*/

#include <stdlib.h>

int     my_strlen(char const*);

char    *clear_nb(char *nb)
{
        int     i = 0;
        int     len = 0;
        int     test = 0;
        char    *new_nb;

        while (nb[i] != '\0'){
                if (test == 0 && (nb[i] == '0' || nb[i] == '-'))
                        i++;
                else if (nb[i] >= '0' && nb[i] <= '9'){
                        len++;
                        i++;
                        test = 1;
                }
        }
        new_nb = malloc(sizeof(char*) * (len + 1));
        new_nb[len + 1] = '\0';
        while (len >= 0){
                new_nb[len] = nb[i];
                len--;
                i--;
        }
        if (new_nb[0] == '\0')
                new_nb[0] = '0';
        return(new_nb);
}

char    *clear_result(char *res)
{
        int     i = my_strlen(clear_nb(res));
        char    *new_res;
        char    *clear_res = clear_nb(res);

        if (res[0] == '-'){
                new_res = malloc(sizeof(char*) * (i + 2));
		new_res[0] = '-';
                new_res[i + 2] = '\0';
                while (i >= 0){
                        new_res[i + 1] = clear_res[i];
                        i--;
                }
        }
        else {
                new_res = malloc(sizeof(char*) * (i + 1));
                new_res[i + 1] = '\0';
                while (i >= 0){
			new_res[i] = clear_res[i];
                        i--;
                }
        }
        return(new_res);
}

char	*clear_par(char *calc)
{
	int	i = my_strlen(calc);
	char	*new_calc = malloc(sizeof(char*) * (i + 1));

	new_calc[i + 1] = 0;
	while (i > 2){
		new_calc[i] = calc[i - 1];
		i--;
	}
	if (calc[i - 1] == '-'){
		new_calc[i] = '0';
		i--;
		new_calc[i] = '0';
		i--;
		new_calc[i] = '-';
	}
	else {
		new_calc[i] = calc[i - 1];
		i--;
		new_calc[i] = '0';
		i--;
		new_calc[i] = '0';
	}
	return (new_calc);
}
