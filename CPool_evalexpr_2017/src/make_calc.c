/*
** EPITECH PROJECT, 2017
** bistro-matic
** File description:
** make calcul for bistro
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "../include/my.h"

int	check_is_neg(char);
int	check_op_is_prio(char);
int	check_op(char);
char	*do_op(char*, char*, char);

char	*new_calcul(char *result, char *calcul, int stop)
{
	int	i = 0;
	int	j = 0;
	int	res_len = my_strlen(result) - 1;
	char	*new_result = malloc(sizeof(char*) * (res_len + 1));

	printf("****new_calc****\n");
	printf("result:     %s\n",result);
	printf("calcul:     %s\n",calcul);
	while (i < stop){
		new_result[i] = calcul[i];
		i++;
	}
	while (result[j] != '\0'){
		new_result[i] = result[j];
		i++;
		j++;
	}
	while (calcul[i] != '\0'){
		new_result[i] = calcul[i];
		i++;
	}
	new_result[i] = 0;
	printf("new_result: %s\n\n",new_result);
	return (new_result);
}

char	*find_summand(char *src)
{
	printf("---***find_summand***\n");
	int	i = 0;
	int	tmp = 0;
	char	*nb;

	if (src[i] == '-')
		i++;
	while (tmp == 0 && src[i] != '\0'){
		i++;
		if (check_op_is_prio(src[i]) == 1)
			tmp = 1;
		else if (src[i] == '+' || src[i] == '-')
			tmp = 1;
	}
	printf("--i: %d\n",i);
	printf("--before_malloc\n");
	nb = malloc(sizeof(char*) * (i+10));
	printf("--after_malloc\n");
	tmp = 0;
	while (tmp < i){
		nb[tmp] = src[tmp];
		tmp++;
	}
	nb[tmp] = 0;
	return (nb);
}

char	*last_calc(char *b_nb1, char *b_nb2, char *calc, int begin_nb1)
{
	char	*nb1 = find_summand(b_nb1);
	char	*nb2 = find_summand(b_nb2);
	int	res_len = my_strlen(nb1) + my_strlen(nb2) + 1;
	char	ope = b_nb2[-1];
	char	*result = malloc(sizeof(char*) * (res_len));
	int i = 0;

	if (ope == '+')
		result = do_op(nb1, nb2, ope);
	else if (ope == '-')
		result = do_op(nb1, nb2, ope);
	free(nb1);
	free(nb2);
	calc = new_calcul(result, calc, begin_nb1);
	free(result);
	return (calc);
}

char	*prio_calc(char *b_nb1, char *b_nb2, char *calc, int begin_nb1)
{
	printf("****prio_calc****\n");
	char	*nb1 = find_summand(b_nb1);
	printf("nb1:  %s\n",nb1);
	char	*nb2 = find_summand(b_nb2);
	printf("nb2:  %s\n",nb2);
	int	res_len = my_strlen(nb1) + my_strlen(nb2) + 1;
	char	ope = b_nb2[-1];
	char	*result = malloc(sizeof(char*) * res_len);

	if (ope == '*')
		result = do_op(nb1, nb2, ope);
	else if (ope == '/')
		result = do_op(nb1, nb2, ope);
	else
		result = do_op(nb1, nb2, ope);
	free(nb1);
	free(nb2);
	calc = new_calcul(result, calc, begin_nb1);
	free(result);
	printf("\n");
	return (calc);
}

char	*calcul(char *calc)
{
	int	i = 0;
	int	nb1 = 0;
	int	nb2 = 0;

	printf("----------------------------------------------------------\n");
	printf("****calcul****\n");
	printf("calc:       %s\n",calc);
	while (calc[i] == '-' && calc[i] == '+')
		i++;
	while (check_op_is_prio(calc[i]) != 1 && calc[i] != '\0'){
		if (calc[i] == '-' && check_is_neg(calc[i]) == 1){
			i++;
			nb1 = i;
		}
		else if (calc[i] == '+' || calc[i] == '-'){
			i++;
			nb1 = i;
		}
		i++;
	}
	if (check_op_is_prio(calc[i]) == 1){
		nb2 = i + 1;
		printf("calc_prio:  %s\n\n",calc);
		calc = calcul(prio_calc(&calc[nb1], &calc[nb2], calc, nb1));
	}
	i = 0;
	nb1 = i;
	if (calc[i] == '-')
		i++;
	while (calc[i] != '+' && calc[i] != '-' && calc[i] != '\0'){
		i++;
	}
	if (calc[i] == '+' || calc[i] == '-'){
		nb2 = i + 1;
		printf("calc_last:  %s\n\n",calc);
		calc = calcul(last_calc(&calc[nb1], &calc[nb2], calc, nb1));
	}
	return (calc);
}
