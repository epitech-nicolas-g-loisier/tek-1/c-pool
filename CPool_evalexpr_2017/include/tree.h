/*
** EPITECH PROJECT, 2017
** tree
** File description:
** headers of tree functions
*/

typedef struct tree
{
	struct tree *left;
	char *nbleft;
	struct tree *right;
	char *nbright;
	char op;
} tree;
