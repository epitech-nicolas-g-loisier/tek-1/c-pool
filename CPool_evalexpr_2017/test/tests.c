#include <criterion/criterion.h>

Test(infin_add, test_more_more)
{
	cr_assert_eq(infin_add("2000000000", "2000000000"), "4000000000");
	cr_assert_eq(infin_add("2", "2"), "4");
	cr_assert_eq(infin_add("10000", "650"), "10650");
	cr_assert_eq(infin_add("9", "9"), "18");
	cr_assert_eq(infin_add("5", "5"), "10");
	cr_assert_eq(infin_add("19", "11"), "30");
}

Test(infin_add, test_less_less)
{
	cr_assert_eq(infin_add("-2000000000", "-2000000000"), "-4000000000");
}

Test(infin_add, test_less_more)
{
}
