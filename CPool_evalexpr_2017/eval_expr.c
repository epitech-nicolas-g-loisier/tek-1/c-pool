/*
** EPITECH PROJECT, 2017
** eval_expr
** File description:
** eval the priority of expr
*/

#include "include/my.h"
#include "include/tree.h"
#include <stdio.h>

//structure d'une parenthese 
typedef struct prteis
{
	char type;//ouvrante ou fermante
	int pos;//position de la parenthese
} prteis;

int nbpar(char *str);
int check_par(char *str);
int posp_open(char *str, int nbp);
int posp_close(char *str, int nbp);

int eval_expr(char *str)
{
	int posp_ouvre;
	int posp_ferme;
	int par = check_par(str);
	int nbp = nbpar(str);
	//creation d'un tableau de structures de parentheses
	prteis *parentheses = malloc(sizeof(prteis) * (nbpar * 2));
	int cpnbp = nbp;
	int cp = 0;
	//recuperation de la postition de chaques parenthèses
	
	while(nbp > 0){
		posp_ouvre = posp_open(str, nbp);
		nbp--;
	}
	nbp = cpnbp;
	while(cp != nbp){
		posp_ferme = posp_close(str, cp);
		cp++;
	}
	//découpage de l'expression et stockage dans un arbre binaire


	int j = 0;
	while(parentheses[j] != NULL){
		
	}
	
}

int main(int ac, char **av)
{
	if(ac == 2)
	{
		my_put_nbr(eval_expr(av[1]));
		my_putchar('\n');
		return (0);
	}
	my_putstr("Erreur de saisie!\n");
	return (84);
}
