/*
** EPITECH PROJECT, 2017
** my_str_to_word_array
** File description:
** split string into words
*/

#include <stdlib.h>
#include <stdio.h>

int my_strlen(char const *str);

int my_isalphanum(char c)
{
	if(c >= 65 && c <= 90 || c >= 97 && c <= 122)
                return (1);
        else if(c >= 48 && c <= 57)
                return (1);
	else
		return (0);

}


int count_nbwords(char const *str, int nbchar)
{
	int i = 0;
	int nbwords = 0;

	while(i < nbchar){
		if(my_isalphanum(str[i]) == 1 && my_isalphanum(str[i - 1]) == 0){
			nbwords++;
			i++;
		}
		else{
			i++;
		}
	}
	return (nbwords);
}

char **my_str_to_word_array(char const *str)
{
	int nbwords = 0;
	int j = 0;
	int i = 0;
	char **array_word;
	int nbchar = my_strlen(str);

	nbwords = count_nbwords(str, nbchar);
	array_word = malloc(sizeof(char*) * nbwords);
	while(i < nbchar){
		if(my_isalphanum(str[i]) == 1){
			array_word[j] = &str[i];
			while(my_isalphanum(str[i]) == 1)				
				i++;
			j++;
		}
		else
			i++;
	}
	array_word[j] = NULL;
	return (array_word);
}
