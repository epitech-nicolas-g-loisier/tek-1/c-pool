/*
** EPITECH PROJECT, 2017
** my_strdup
** File description:
** allocate memory to the strings
*/

#include <stdlib.h>
#include <stdio.h>

char *my_strdup(char const *src)
{
	char *newstr;
	int i = 0;

	while(src[i] != '\0'){
		i++;
	}
	newstr = malloc(sizeof(char) * i);
	i = 0;
	while(src[i] != '\0'){
		newstr[i] = src[i];
		i++;
	}
	newstr[i] = '\0';
	return (newstr);
}
