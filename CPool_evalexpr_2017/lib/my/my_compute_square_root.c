/*
** EPITECH PROJECT, 2017
** my_compute_square_root
** File description:
** compute square roor
*/

int my_compute_square_root(int nb)
{
	int result = 0;

	if(nb < 0){
		return (0);
	}
	while(result*result < nb){
		result++;
	}
	if(result*result != nb){
		return (0);
	}
	else
		return (result);
}
