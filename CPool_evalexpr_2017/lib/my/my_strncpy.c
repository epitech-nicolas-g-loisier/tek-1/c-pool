/*
** EPITECH PROJECT, 2017
** my_strncpy
** File description:
** copy n character from a string into another
*/

char *my_strncpy(char *dest, char const *src, int n)
{
	int nbchar = 0;
	int counter = 0;

	while(src[nbchar] != 0){
		nbchar++;
	}
	if(nbchar > n){
		while(src[counter] != '\0'){
			dest[counter] = src[counter];
			counter++;
		}
	}
	else{
		while(counter <= n){
			dest[counter] = src[counter];
			counter++;
		}
	}
	return (dest);
}
