/*
** EPITECH PROJECT, 2017
** my_getnbr
** File description:
** get number
*/

int my_compute_power_rec(int nb, int p);

int my_strlen(char const *str);

int my_isnum(char c)
{
	if(c >= 48 && c <= 57)
		return (1);
	else
		return (0);
}

int intlen(char const *str, int nbchar, int nbint)
{
	int i = 0;
	long number = 0;
	
	while(i < nbchar){
		if(my_isnum(str[i]) == 1){
                        nbint--;
                        number = number + (str[i] - 48) * my_compute_power_rec(10, nbint);
                        if(my_isnum(str[i + 1]) == 0)
                                i = nbchar;
	        }
                i++;
        }
	if(number > 2147483647)
		return (0);
	else
		return (1);
}

int signe_nbint(char const *str, int *nbchar, int *negatif)
{
	int i = 0;
	int nbint = 0;

	while(i < *nbchar){
                if(my_isnum(str[i]) == 1){
                        if(str[i - 1] == 45)
                                *negatif = -1;
                        nbint++;
                        if(my_isnum(str[i + 1]) == 0)
                                i = *nbchar;
                }
                i++;
        }
	return (nbint);
}

int my_getnbr(char const *str)
{
	int i = 0;
	int nbchar = my_strlen(str);
	int negatif = 1;
	int number = 0;
	int nbint = signe_nbint(str, &nbchar, &negatif);
	
	if(intlen(str, nbchar, nbint) == 0)
		return (0);
	while(i < nbchar){
		if(my_isnum(str[i]) == 1){
			nbint--;
			number = number + (str[i] - 48) * my_compute_power_rec(10, nbint);
			if(my_isnum(str[i + 1]) == 0)
                                i = nbchar;
		}
		i++;
	}
	number = number * negatif; 
	return (number);
}
