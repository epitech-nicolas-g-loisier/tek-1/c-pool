/*
** EPITECH PROJECT, 2017
** my_strcapitalize
** File description:
** capitalize the first letter of each word
*/

char *my_strcapitalize(char *str)
{
	int i = 0;
	int j = 0;

	while(str[j] != '\0'){
		if(str[j] >= 65 && str[j] <= 90)
			str[j] = str[j] + 32;
		j++;
	}
	if(str[0] >= 97 && str[0] <= 122)
		str[0] = str[0] - 32;
	while(str[i] != '\0'){
		if(str[i] == ' ' || str[i] == '-' || str[i] == '+'){
			if(str[i + 1] >= 97 && str[i + 1] <= 122)
				str[i + 1] = str[i + 1] - 32;
		}
		i++;
	}
	return (str);
}
