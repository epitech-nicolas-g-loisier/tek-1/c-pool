/*
** EPITECH PROJECT, 2017
** my_strncmp
** File description:
** compare two strings on the first n byte of strings
*/

int my_strncmp(char const *s1, char const *s2, int n)
{
	int i = 0;
	int diff = 0;

	while(i < n){
		if(s1[i] != s2[i]){
			diff = s1[i] - s2[i];
			return (diff);
		}
		i++;
	}
	return (0);
}
