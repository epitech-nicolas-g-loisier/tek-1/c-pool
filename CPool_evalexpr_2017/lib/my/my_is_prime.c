/*
** EPITECH PROJECT, 2017
** my_is_prime
** File description:
** find a prime number
*/

int my_is_prime(int nb)
{
	int i = 2;

	if(nb == i)
		return (1);
	if(nb%2 == 0 || nb < i){
		return (0);
	}
	else{
		while(i <= nb){
			i++;
			if(nb%i == 0 && i == nb){
				return (1);
			}
			else if(nb%i == 0){
				return (0);
			}
		}
		return (0);
	}
}
