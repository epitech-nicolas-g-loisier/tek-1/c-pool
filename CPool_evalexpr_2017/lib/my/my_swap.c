/*
** EPITECH PROJECT, 2017
** my_swap
** File description:
** swap
*/

void my_swap(int *a, int *b)
{
	int c;

	c = *b;
	*b = *a;
	*a = c;
}
