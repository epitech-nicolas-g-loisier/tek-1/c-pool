/*
** EPITECH PROJECT, 2017
** my_str_isalpha
** File description:
** verify if string only contain alphabetical characters
*/

int my_str_isalpha(char const *str)
{
	int i = 0;
	int alpha = 0;

	while(str[i] != '\0'){
		if(str[i] >= 65 && str[i] <= 90){
			i++;
		}
		else if(str[i] >= 97 && str[i] <= 122){
			i++;
		}
			return (0);
	}
	return (1);
}
