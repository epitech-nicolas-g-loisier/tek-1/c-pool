/*
** EPITECH PROJECT, 2017
** my_find_prime_sup
** File description:
** find prime sup
*/

int my_find_prime_sup(int nb)
{
	int div = 2;
	int prime = nb;

	if(nb <= 2){
		return (2);
	}
	while(div < prime){
		if(prime / div * div == prime){
			div = 2;
			prime++;
		}
		div++;
		if(prime == 2147483647)
			return (0);
	}
	return (prime);
}
