/*
** EPITECH PROJECT, 2017
** my_putstr
** File description:
** put str in parameter
*/

void my_putchar(char c);

int my_putstr(char const *str)
{
	int i = 0;
	int nbchr = 0;

	while(str[nbchr] != '\0'){
		nbchr++;
	}
	while(i < nbchr){
		my_putchar(str[i]);
		i++;
	}
	return (0);
}
