/*
** EPITECH PROJECT, 2017
** my_strncat
** File description:
** concatenates n characters
*/

char	*my_strncat(char *dest, char const *src, int nb)
{
	int i = 0;
	int j = 0;

	while (dest[i] != '\0') {
		i++;
	}
	while (j < nb) {
		dest[i] = src[j];
		j++;
		i++;
	}
	dest[i] = '\0';
	return (dest);
}
