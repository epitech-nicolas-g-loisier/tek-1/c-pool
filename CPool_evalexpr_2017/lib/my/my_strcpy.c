/*
** EPITECH PROJECT, 2017
** MY_STRCPY
** File description:
** copy a string into another
*/

char *my_strcpy(char *dest, char const *src)
{
	int counter = 0;
	int counter_char = 0;

	while(src[counter] != '\0'){
		counter++;
	}
	while(counter_char <= counter){
		dest[counter_char] = src[counter_char];
		counter_char++;
	}
	return (dest);
}
