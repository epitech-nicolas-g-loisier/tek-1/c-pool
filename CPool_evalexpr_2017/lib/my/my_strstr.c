/*
** EPITECH PROJECT, 2017
** my_strstr
** File description:
** find str in str
*/

#include <stdio.h>

char *my_strstr(char const *str, char const *to_find)
{
	int counter = 0;
	int counter_tofind = 0;

	if(str[0] == to_find[0] || to_find[0] == '\0')
		return (str);
	while(str[counter] != '\0'){
		if(str[counter] == to_find[counter_tofind])
			counter_tofind++;
		else{
			counter = counter - counter_tofind;
			counter_tofind = 0;
		}
		counter++;
		if(to_find[counter_tofind + 1] == '\0'){
			if(str[counter] == to_find[counter_tofind]){
				return (&str[counter - counter_tofind]);
			}
		}
	}
	return (NULL);
}
