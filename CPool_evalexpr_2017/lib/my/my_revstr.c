/*
** EPITECH PROJECT, 2017
** my_revstr
** File description:
** reverse a string
*/

char *my_revstr(char *str)
{
	int nbchar = 0;
	int counter_char = 0;
	int counter_charj = 0;

	while(str[nbchar] != '\0'){
		nbchar++;
	}
	char newstr[nbchar];
	while(counter_char < nbchar){
		newstr[counter_char] = str[counter_char];
		counter_char++;
	}
	nbchar = nbchar - 1;
	while(nbchar >= 0){
		str[counter_charj] = newstr[nbchar];
		nbchar--;
		counter_charj++;
	}
	return (str);
}
