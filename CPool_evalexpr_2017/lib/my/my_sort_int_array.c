/*
** EPITECH PROJECT, 2017
** my_sort_int_array
** File description:
** sort int array
*/

void my_sort_int_array(int *tab, int size)
{
	int i = 0;
	int save = 0;

	while(i < (size - 1)){
		if(tab[i] > tab[i + 1]){
			save = tab[i];
			tab[i] = tab[i + 1];
			tab[i + 1] = save;
			i = 0;
		}
		else
			i++;
	}
}
