/*
** EPITECH PROJECT, 2017
** do-po
** File description:
** do
*/

#include "../include/my.h"

int getop(char *str)
{
	char op[5] = "+-*/%";
	int i = 0;
	int j = 0;

	while(str[i] != '\0'){
		while(op[j] != '\0'){
			if(str[i] == op[j]){
				return j;
			}
			j++;
		}
		j = 0;
		i++;
	}
	return 0;
}

int do_op(int ac, char **av)
{
	int nb1 = my_getnbr(av[1]);
	int nb2 = my_getnbr(av[3]);

	int (*fadd)(int, int);
	fadd = &add;
	int (*fless)(int, int);
	fless = &less;
	int (*fmulti)(int, int);
	fmulti = &multi;
	int (*fdiv)(int, int);
	fdiv = &div;
	int (*fmod)(int, int);
	fmod = &mod;
	int (* opr[5])(int, int);
	opr[0] = fadd;
	opr[1] = fless;
	opr[2] = fmulti;
	opr[3] = fdiv;
	opr[4] = fmod;
	char op[5] = "+-*/%";
	return (opr[getop(av[2])](nb1, nb2));
	return 0;
}
