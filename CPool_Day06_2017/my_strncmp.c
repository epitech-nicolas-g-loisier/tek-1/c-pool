/*
** EPITECH PROJECT, 2017
** my_strncmp
** File description:
** strcmp
*/

int	my_strncmp(char const *s1, char const *s2, int n)
{
	int	i = n;

	while (s1[i] == s2[i]){
		if (s1[i] == '\0')
			return(0);
		i++;
	}
	if(s1[i] < s1[i])
		return(-1);
	else
		return(1);
}

void	main()
{
	printf("%d",strcmp("hello","hallo", 3));
     	printf("%d",strcmp("hello","hallo", 3));
}
		
