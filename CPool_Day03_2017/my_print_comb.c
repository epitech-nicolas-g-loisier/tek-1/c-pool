/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_print_comb.c
*/

int condition(int a,int b,int c)
{
	my_putchar (a + 48);
	my_putchar (b + 48);
	my_putchar (c + 48);
	if (a == 9)
		my_putchar (10);
	else if (a == 7 && b == 8 && c == 9)
		my_putchar ('\n');
	else{
		my_putchar (',');
		my_putchar (' ');
	}
}

int my_print_comb (void)
{
	int	a = 0;
	int	b = 0;
	int	c = 0;

       	while (a <= 9){
		b = a + 1;
		while (b <= 9){
			c = b + 1;
			while (c <= 9){
				condition(a, b, c);
				c = c + 1;
			}
			b = b + 1;
		}
		a = a + 1;
	}
	return (0);
}
