/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_print_alpha.c
*/

int	my_print_alpha(void)
{
	int	l;

	l = 97;
	while (l <= 122) {
		my_putchar(l);
		l = l + 1;
	}
	my_putchar('\n');
	return (0);
}
