/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_print_digits
*/

int	my_print_digits(void)
{
	int	nb;

	nb = '0';
       	while (nb <= '9'){
       		my_putchar(nb);
       		nb = nb + 1;
       	}
	my_putchar('\n');
       	return (0);
}
