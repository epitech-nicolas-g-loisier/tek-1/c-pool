/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_print_revalpha
*/

int	my_print_revalpha(void)
{
	int	l;

	l = 122;
	while (l >= 97){
	      	my_putchar(l);
		l = l - 1;
	}
	my_putchar('\n');
	return (0);
}
