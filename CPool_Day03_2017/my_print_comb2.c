/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_print_comb2.c
*/

int test(int a,int b,int c,int d)
{
	if (b <= d && a <= c && a + b != c + d) {
		my_putchar (a + 48);
		my_putchar (b + 48);
		my_putchar (32);
		my_putchar (c + 48);
		my_putchar (d + 48);
		if (a == 9 && b == 8 && c == 9 && d == 9)
			my_putchar (10);
		else{
			my_putchar (44);
			my_putchar (32);
		}
	}
}
int	my_print_comb2(void)
{
	int	a = 0;
	int	b;
	int	c;
	int	d;

	while (a <= 9){
		b = 0;
		while (b <= 8){
			c = 0;
			while (c <= 9){
				d = 0;
				while (d <= 9){
					test(a, b, c, d++);
					}
				c = c + 1;
			}
			b = b + 1;
		}
		a = a + 1;
	}
}
