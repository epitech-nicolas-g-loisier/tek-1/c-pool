/*
** EPITECH PROJECT, 2017
** test
** File description:
** test
*/

#include <string.h>
#include <criterion/criterion.h>

Test(sample, test) {
	cr_expect(strlen("TEST") == 4, "Expected");
	cr_expect(strlen("HELLO") == 4, "This will always fail");
	cr_assert(strlen("") == 0);
}
