/*
** EPITECH PROJECT, 2017
** final_stumper
** File description:
** bonus
*/

#include "../include/my.h"

void	shape(int x, int y)
{
	if (x == y){
		my_putstr(" || [square] ");
		my_put_nbr(x);
		my_putchar(' ');
		my_put_nbr(y);
	}
	my_putstr(" || [rectangle] ");
	my_put_nbr(x);
	my_putchar(' ');
	my_put_nbr(y);
	my_putchar('\n');
}
