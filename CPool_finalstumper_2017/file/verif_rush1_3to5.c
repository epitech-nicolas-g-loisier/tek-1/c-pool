/*
** EPITECH PROJECT, 2017
** final_stumper
** File description:
** 
*/

#include "include/my.h"

void	print_the_3(int x, int y)
{
	my_putstr("[rush1-3] ");
	my_put_nbr(x);
	my_putchar(' ');
	my_put_nbr(y);
	my_putstr(" || [rush1-4] ");
	my_put_nbr(x);
	my_putchar(' ');
	my_put_nbr(y);
	my_putstr(" || [rush1-5] ");
	my_put_nbr(x);
	my_putchar(' ');
	my_put_nbr(y);
}

void	print1_3(int x, int y)
{
	my_putstr("[rush1-3] ");
	my_put_nbr(x);
	my_putchar(' ');
	my_put_nbr(y);
}

void	print1_4(int x, int y)
{
	my_putstr("[rush1-4] ");
	my_put_nbr(x);
	my_putchar(' ');
	my_put_nbr(y);
}

void	print1_5(int x, int y)
{
	my_putstr("[rush1-5] ");
	my_put_nbr(x);
	my_putchar(' ');
	my_put_nbr(y);
}

void	find(int x, int y, char *buff)
{
	int	i = 0;
	int	stop = 0;
	int	A = 0;
	int	C = 0;

	while (buff[i] != '\0' && stop != 1){
		if (buff[i] == 'A')
			A++;
		else if (buff[i] == 'C')
			C++;
		i++;
		if (A == 2 || C == 2)
			stop = 1;
	}
	if (C == 2)
		print1_5(x, y);
	else if (A == 2 && i == x)
		print1_3(x, y);
	else
		print1_4(x, y);
}
