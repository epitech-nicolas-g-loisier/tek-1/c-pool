/*
** EPITECH PROJECT, 2017
** final_stumper
** File description:
** find the rush
*/

#include "include/my.h"
#include <unistd.h>

void	find(int, int, char*);
void	print_the_3(int, int);

int	str_isrush1_1(char *buff)
{
	int     i = 0;

	while (buff[i] != '\0'){
		if (buff[i] == 'o' || buff[i] == '-' || buff[i] == '|')
			i++;
		else if (buff[i] == '\n' || buff[i] == ' ')
			i++;
		else
			return (0);
	}
	return (1);
}

int	str_isrush1_2(char *buff)
{
	int     i = 0;

	while (buff[i] != '\0'){
		if (buff[i] == '/' || buff[i] == '\\' || buff[i] == '*')
			i++;
		else if (buff[i] == '\n' || buff[i] == ' ')
			i++;
		else
			return (0);
	}
	return (1);
}

void	verif_rush(int x, int y, char *buff)
{
	if ((x == 0 && y == 0) || (x == 1 || y == 1)){
		print_the_3(x, y);
	}
	else
		find(x, y, buff);
}

void	find_rush(int x, int y, char *buff)
{
	if (my_str_isalpha(buff) == 1)
		verif_rush(x, y, buff);
	else if (str_isrush1_1(buff) == 1){
		my_putstr("[rush1-1] ");
		my_put_nbr(x);
		my_putchar(' ');
		my_put_nbr(y);
	}
	else {
		if (str_isrush1_2(buff) == 1){
			my_putstr("[rush1-2]");
			my_put_nbr(x);
			my_putchar(' ');
			my_put_nbr(y);
		}
		else
			write(2, "invalid square\n", 15);
	}
}
