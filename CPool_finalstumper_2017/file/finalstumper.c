/*
** EPITECH PROJECT, 2017
** finalstumper
** File description:
** rush3
*/
#include <unistd.h>
#define BUFF_SIZE (4096)

int	count_lw(char*);

void	rush3(char *buff)
{
	count_lw(buff);
}

int	main()
{
	char buff[BUFF_SIZE + 1];
	int offset;
	int len;

	offset = 0;
	while ((len = read(0, buff + offset, BUFF_SIZE - offset)) > 0)
		offset = offset + len;
	if (len < 0 || offset >= 4096){
		write(2,"none\n", 5);
		return (84);
	}
	buff[offset] = '\0';
	rush3(buff);
	return (0);
}
