/*
** EPITECH PROJECT, 2017
** countlw
** File description:
** countlw
*/

#include <unistd.h>
#include "../include/my.h"

void	find_rush(int, int, char*);
void    shape(int, int);

int	count_lw(char *buff)
{
	int count_buff = 0;
	int countx = 0;
	int county = 0;

	while(buff[count_buff] != '\0') {
		countx = 0;
		while(buff[count_buff] != '\n') {
			countx = countx + 1;
			count_buff = count_buff + 1;
		}
		county = county + 1;
		count_buff = count_buff + 1;
	}
	if (countx == 0 || county == 0){
		write(2, "none\n", 5);
		return(84);
	}
	find_rush(countx, county, buff);
	shape(countx, county);
	return (0);
}
