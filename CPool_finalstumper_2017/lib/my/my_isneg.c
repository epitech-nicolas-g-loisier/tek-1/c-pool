/*
** EPITECH PROJECT, 2017
** my_isneg
** File description:
** task04
*/

void	my_putchar(char);

int	my_isneg(int nb)
{
	char printn;
	char printp;

	printn = 'N';
	printp = 'P';
	if(nb<0)
	{
		my_putchar(printn);
	}
	else
	{
		my_putchar(printp);
	}
	my_putchar('\n');
	return(0);
}
