/*
** EPITECH PROJECT, 2017
** my_strncmp
** File description:
** task06
*/

int	my_strncmp(char const *s1, char const *s2, int n)
{
	int count = 0;
	int dif;

	dif = s1[count] - s2[count];
	while ((s1[count] - s2[count] == dif) && (count < n)) {
		count = count + 1;
	}
	count = count - 1;
	dif = s1[count] - s2[count];
	return(dif);
}
