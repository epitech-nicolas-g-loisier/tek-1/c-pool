/*
** EPITECH PROJECT, 2017
** my_strncpy
** File description:
** task02
*/

char	*my_strncpy(char *dest, char const *src, int n)
{
	int count = 0;

	while ((src[count] != '\0') && count < n) {
		dest[count] = src[count];
		count = count + 1;
	}
	return(dest);
}
