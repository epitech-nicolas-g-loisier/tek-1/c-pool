/*
** EPITECH PROJECT, 2017
** my_strcat
** File description:
** task02
*/

char	*my_strcat(char *dest, char const *src)
{
	int count = 0;
	int add_count = 0;

	while (dest[count] != '\0') {
		count = count + 1;
	}
	while (src[add_count] != '\0') {
		dest[count] = src[add_count];
		count = count + 1;
		add_count = add_count + 1;
	}
	dest[count] = '\0';
}
