/*
** EPITECH PROJECT, 2017
** my_is_prime
** File description:
** task06
*/

int	my_is_prime(int nb)
{
	int count = 1;
	int calc = 0;
	int calc0 = 0;
	long test = nb;

	if ((test > 2147483648) || (test < -2147483647)) {
		return(0);
	}
	while (count <= nb) {
		calc = nb % count;
		if (calc == 0) {
			calc0 = calc0 + 1;
		}
		count = count + 1;
	}
	if (calc0 == 2) {
		return(1);
	}
	else {
		return(0);
	}	
}
