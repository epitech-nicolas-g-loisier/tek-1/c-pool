/*
** EPITECH PROJECT, 2017
** my_strcpy
** File description:
** task01
*/

char	*my_strcpy(char *dest, char const *src)
{
	int count = 0;

	while (src[count] != '\0') {
		dest[count] = src[count];
		count = count + 1;
	}
	return(dest);
}
