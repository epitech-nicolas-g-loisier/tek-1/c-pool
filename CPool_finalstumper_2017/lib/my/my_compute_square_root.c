/*
** EPITECH PROJECT, 2017
** my_compute_square_root
** File description:
** task05
*/

int	my_compute_square_root(int nb)
{
	int a = 1;
	int b;
	long test = nb;
	
	if ((test < -2147483647) || (test > 2147483648)) {
		return(0);
	}
	while ((b != 1) && (nb >= 0)) {
		b = nb / a;
		if (b == a) {
			return (a);
		}
		a = a + 1; 
	}
	return (0);
}
