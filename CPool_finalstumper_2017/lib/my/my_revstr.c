/*
** EPITECH PROJECT, 2017
** my_revstr
** File description:
** task03
*/

char	*my_revstr(char *str)
{
	int count = 0;
	int modulo;
	int end;
	char save;

	while (str[count] != '\0') {
		count = count + 1;
	}
	count = count - 1;
	end = count;
	count = 0;
	while (count < end) {
		save = str[end];
		str[end] = str[count];
		str[count] = save;
		end = end - 1;
		count = count + 1;
	}
	return(str);
}
