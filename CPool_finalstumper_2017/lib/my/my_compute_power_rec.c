/*
** EPITECH PROJECT, 2017
** my_compute_power_rec
** File description:
** task04
*/

int	my_compute_power_rec(int nb, int p)
{
	int a = 1;
	long test = nb;

	if ((test < -2147483647) || (test > 2147483648)) {
		return(0);
	}
	if (p == 0) {
		return (1);
	}
	else if (p < 0) {
		return (0);
	}
	else {
		nb = nb * my_compute_power_rec(nb, p - 1);
		return (nb);
	}
}
