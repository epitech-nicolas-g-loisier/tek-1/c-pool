/*
** EPITECH PROJECT, 2017
** my_strlen
** File description:
** task03
*/

int	my_strlen(char const *str)
{
	int i = 0; 

	while (*str != '\0') {
		str = str + 1;
		i = i + 1;
	}
	return (i);
}
