/*
** EPITECH PROJECT, 2017
** my_strcmp
** File description:
** task05
*/

int	my_strcmp(char const *s1, char const *s2)
{
	int count = 0;
	int dif;

	dif = s1[count] - s2[count];
	while (s1[count] - s2[count] == dif) {
		count = count + 1;
	}
	dif = s1[count] - s2[count];
	return(dif);
}
