/*
** EPITECH PROJECT, 2017
** my_strncat
** File description:
** task03
*/

char	*my_strncat(char *dest, char const *src, int nb)
{
	int count = 0;
	int add_count = 0;

	while (dest[count] != '\0') {
		count = count + 1;
	}
	while (add_count <= nb) {
		dest[count] = src[add_count];
		count = count + 1;
		add_count = add_count + 1;
	}
	dest[count] = '\0';
}
