/*
** EPITECH PROJECT, 2017
** my_strlowcase
** File description:
** task08
*/

char	*my_strlowcase(char *str)
{
	int count = 0;

	while (str[count] != '\0') {
		if ((str[count] >= 'A') && (str[count] <= 'Z'))
			str[count] = str[count] + 32;
		count = count + 1;
	}
	return(str);
}
