/*
** EPITECH PROJECT, 2017
** final_ stumper : my_str_isalpha
** File description:
** search rush1-3 to rush1-5
*/

int	my_str_isalpha(char const *str)
{
	int	i = 0;

	while (str[i] != '\0'){
		if (str[i] > 64 && str[i] < 91)
			i++;
		else if (str[i] == '\n' || str[i] == ' ')
			i++;
		else
			return (0);
	}
	return (1);
}
