/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_put_nbr.c
*/

void	my_putchar(char c);

int	my_put_nbr(int nb)
{
	int	c;
	int	div = 1;
	int	nbr;

	if (nb < 0){
		nb = -nb;
		my_putchar('-');
	}
	nbr = nb;
	while (nb >= 10){
		nb = nb / 10;
		div = div * 10;
	}
	while (div != 0){
		c = nbr / div;
		nbr = nbr - c * div;
		div = div / 10;
		my_putchar(c+48);
	}
	return (0);
}
