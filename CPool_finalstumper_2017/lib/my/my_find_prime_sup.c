/*
** EPITECH PROJECT, 2017
** my_find_prime_sup.c
** File description:
** task07
*/

int	my_find_prime_sup(int nb)
{
	int count = 1;
	int calc = 0;
	int calc0 = 0;
	long test = nb;

	if ((test > 2147483648) || (test < -2147483647)) {
		return(0);
	}
	while (count <= nb) {
		calc = nb % count;
		if (calc == 0) {
			calc0 = calc0 + 1;
		}
		count = count + 1;
	}
	if (calc0 == 2) {
		return(nb);
	}
	my_find_prime_sup(nb + 1);
}
