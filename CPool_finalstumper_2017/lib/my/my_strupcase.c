/*
** EPITECH PROJECT, 2017
** my_strupcase
** File description:
** task07
*/

char	*my_strupcase(char *str)
{
	int count = 0;

	while (str[count] != '\0') {
		if ((str[count] >= 'a') && (str[count] <= 'z'))
			str[count] = str[count] - 32;
		count = count + 1;
	}
	return(str);
}
