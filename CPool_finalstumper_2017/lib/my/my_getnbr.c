/*
** EPITECH PROJECT, 2017
** my_getnbr
** File description:
** task05_day_04
*/

int	return_nbr(int count_dash, int nbr)
{
	long result = 0;
	
	if (count_dash == 1)
		nbr = nbr * -1;
	result = nbr;
	if ((result < -2147483647) || (result > 2147483648))
		return(0);
	return(nbr);
}

int	my_getnbr(char const *str)
{
	int count = 0;
	int count_dash = 0;
	int nbr = 0;

	while ((str[count] < '0') || (str[count] > '9')) {
		 if (str[count] == 45) 
			 count_dash = count_dash + 1;
		 else
			 return(0);
		 count = count + 1;
	}
	count_dash = count_dash % 2;
	while ((str[count] >= '0') && (str[count] <= '9')) {
		nbr = nbr * 10;
		nbr = nbr + (str[count] - 48);
		count = count + 1;
	}
	return_nbr(count_dash, nbr);
}
