/*
** EPITECH PROJECT, 2017
** Day13 task01
** File description:
** open window
*/

#include <SFML/Graphics/Export.h>
#include <SFML/Graphics/Color.h>
#include <SFML/Graphics/Rect.h>
#include <SFML/Graphics/Types.h>
#include <SFML/Graphics/PrimitiveType.h>
#include <SFML/Graphics/RenderStates.h>
#include <SFML/Graphics/Vertex.h>
#include <SFML/Window/Event.h>
#include <SFML/Window/VideoMode.h>
#include <SFML/Window/WindowHandle.h>
#include <SFML/Window/Window.h>
#include <SFML/System/Vector2.h>
#include <stddef.h>
#include <SFML/Audio.h>
#include <SFML/Graphics.h>
#include <SFML/Window/Types.h>
#include <SFML/System/InputStream.h>
#include <stddef.h>

int	main(void)
{
	sfVideoMode	mode = {800,600, 32};
	sfRenderWindow*	window;
	sfEvent		event;
	sfTexture*	texture;
	sfSprite*	sprite;

	window = sfRenderWindow_create(mode, "chocolatine",sfClose, NULL);
	if (!window)
		return 84;
	texture = sfTexture_createFromFile("../../Desktop/théo.jpg", NULL);
	if (!texture)
		return 84;
	sprite = sfSprite_create();
	sfSprite_setTexture(sprite, texture, sfTrue);
	while (sfRenderWindow_isOpen(window))
	{
		/* Process events */
		while (sfRenderWindow_pollEvent(window, &event))
		{
			/* Close window : exit */
			if (event.type == sfEvtClosed)
				sfRenderWindow_close(window);
		}
	}
	return 0;
}
