/*
** EPITECH PROJECT, 2017
** Rush1
** File description:
** rush1-4
*/

#include <unistd.h>

void	my_putchar(char c);

void	m_error (char c)
{
	write(2, &c, 1);
}

void	error(char *str)
{
	int	i = 0;

	while (str[i] != '\0'){
		m_error(str[i]);
		i++;
	}
}

int	condition(int x, int y)
{
	int	count = 0;

	if (y <= 0 || x <= 0){
		error("Invalid size\n");
	}
	else if (y == 1){
		while (count < x){
			my_putchar('B');
			count++;
		}
		my_putchar(10);
	}
	else {
		while (count < y){
			my_putchar('B');
			my_putchar(10);
			count++;
		}
	}
	return (0);
}

void	rush_first_last(int x, int y)
{
	int	columnsb = 0;

		my_putchar('A');
		while (columnsb < x - 2){
			my_putchar('B');
			columnsb++;
		}
		my_putchar('C');
		my_putchar(10);
}

void	rush(int x, int y)
{
	int space = 0;
	int columns = 0;

	if (x <= 1 || y <= 1)
		condition(x, y);
	else{
		rush_first_last(x, y);
		while (columns < y - 2){
			space = 0;
			my_putchar('B');
			while (space < x - 2){
				my_putchar(32);
				space++;
			}
			my_putchar('B');
			my_putchar(10);
			columns++;
		}
		rush_first_last(x, y);
	}
}

int	main()
{
	rush(30, 30);
	return (0);
}
