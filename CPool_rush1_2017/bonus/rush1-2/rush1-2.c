/*
** EPITECH PROJECT, 2017
** rush1-2
** File description:
** 
*/

#include <unistd.h>

void	my_putchar(char c)
{
	write(1, &c, 1);
}

void	m_error (char c)
{
	write(2, &c, 1);
}

void	error(char *str)
{
	int	i = 0;

	while (str[i] != '\0') {
		m_error(str[i]);
		i++;
	}
}

int	condition(int x, int y)
{
	int	count = 0;

	if (y <= 0 || x <= 0) {
		error("Invalid size\n");
	}
	else if (y == 1) {
		while (count < x){
			my_putchar('*');
			count++;
		}
		my_putchar(10);
	}
	else {
		while (count < y){
			my_putchar('*');
			my_putchar(10);
			count++;
		}
	}
	return (0);
}

void	rush_first(int x, int y)
{
	int	columnsb = 0;
	
	my_putchar('/');
	while (columnsb < x - 2) {
		my_putchar('*');
		columnsb++;
	}
	my_putchar('\\');
	my_putchar(10);
}

void	rush_last(int x, int y)
{
	int	columnsa = 0;

	my_putchar('\\');
	while (columnsa < x - 2) {
		my_putchar('*');
		columnsa++;
	}
	my_putchar('/');
	my_putchar(10);
}

void	rush(int x, int y)
{
	int space = 0;
	int columns = 0;

	if (x <= 1 || y <= 1)
		condition(x, y);
	else{
		rush_first(x, y);
		while (columns < y - 2) {
			space = 0;
			my_putchar('*');
			while (space < x - 2) {
				my_putchar(32);
				space++;
			}
			my_putchar('*');
			my_putchar(10);
			columns++;
		}
		rush_last(x, y);
	}
}

int	main(void)
{
	rush(5, 5);
	return (0);
}
