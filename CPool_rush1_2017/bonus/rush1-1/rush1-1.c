/*
** EPITECH PROJECT, 2017
** rush1-1
** File description:
** Sujet 1
*/

#include <unistd.h>

void	my_putchar(char c);

void	my_putchar(char c)
{
	write(1, &c, 1);
}

void	m_error (char c)
{
	write(2, &c, 1);
}

void	error(char *str)
{
	int	i = 0;

	while (str[i] != '\0'){
		m_error(str[i]);
		i++;
	}
}

int	condition(int x, int y)
{
	error("Invalid size\n");
	return (0);
}

int	condition_two(int x, int y)
{
	int	count = 0;

	my_putchar('o');
	if ( y == 1 && x == 1)
		return(0);
	else if (y == 1) {
		while (count < x) {
			my_putchar('-');
			count++;
		}
	}
	else {
		while (count < y) {
			my_putchar('|');
			count++;
			my_putchar(10);
		}
	}
	my_putchar('o');
	my_putchar(10);
	return (0);
}

void	rush_first_last(int x, int y)
{
	int	columnsb = 0;

		my_putchar('o');
		while (columnsb < x - 2) {
			my_putchar('-');
			columnsb++;
		}
		my_putchar('o');
		my_putchar(10);
}

void	rush(int x, int y)
{
	int space = 0;
	int columns = 0;

	if (x <= 0 || y <= 0)
		condition(x, y);
	else if (x == 1 || y == 1)
		condition_two(x, y);
	else{
		rush_first_last(x, y);
		while (columns < y - 2) {
			space = 0;
			my_putchar('|');
			while (space < x - 2) {
				my_putchar(32);
				space++;
			}
			my_putchar('|');
			my_putchar(10);
			columns++;
		}
		rush_first_last(x, y);
	}
}

int	main(void)
{
	rush(5, 5);
	return(0);
}
