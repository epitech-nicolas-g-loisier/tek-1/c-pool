/*
** EPITECH PROJECT, 2017
** my_print_params
** File description:
** print_params
*/

void	my_putchar(char c);
int	my_putstr(char const *str);

void	my_print_params(char *str)
{
	my_putstr(str);
}

int     main(int argc, char **argv)
{
        int     i = 0;

        while (i < argc){
                my_print_params(argv[i]);
                i++;
        }
        return (0);
}
