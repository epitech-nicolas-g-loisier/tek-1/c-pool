/*
** EPITECH PROJECT, 2017
** macro
** File description:
** macros of errors messages and values
*/

/*
**Macros of errors
*/

#define SYNTAX_ERROR_MSG	"syntax error\n"
#define ERROR_MSG		"error\n"
#define BASE_ERROR_MSG		"Invalid base\n"
#define ERROR 84
#define BASE_ERROR 84

/*
**Macros of values
*/

#define DECIMAL 10
#define EXPR_SIZE 4096
#define NUM_SIZE 10
#define OP_SIZE 10
