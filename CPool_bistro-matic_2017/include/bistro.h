/*
** EPITECH PROJECT, 2017
** macro
** File description:
** macros of errors messages and values
*/

/*
**Prototypes of infinity operations
*/
char *mult_cond(char *nb1, char *nb2);
char *infin_add(char *nb1, char *nb2);
char *infin_div(char *nb1, char *nb2);
char *infin_mod(char *nb1, char *nb2);

/*
**Prototypes of infin_less for infin_add
*/
char *compute_add(char *nb1, char *nb2, char *res, int lenght);
char *infin_less(char *nb1, char *nb2, char *res, int lenght, int biggest);
int isnegorpos(int biggest, int negatif);
int check_error(char *nb1, char *nb2);

/*
**Prototypes of char_functions
*/
int getint(char c);
int my_isupper(char c);
int my_islower(char c);
int contain(char c, char *str);

/*
**Prototypes of string_functions
*/
char *deletezero(char *res);
char *adapt(char *str, int size_init, int size_final);
char *shift(char *str);
char *lessbefore(char *res, int lenght);

/*
**Prototypes for management of bases
*/
char *convert_base(char *expr, char *base_num, char *base_op);
int check_base(char *base_num, char *base_op);
char *restore_syntax(char *expr, char *base_num, char *base_op);
char *convertbase(char *expr, int base);
char *my_compute_power_string(int nb, int p);

char    *chang_sign(char *nbr);
