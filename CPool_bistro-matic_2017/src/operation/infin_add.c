/*
** EPITECH PROJECT, 2017
** Bistro-matic
** File description:
** infin_add for bistro
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../../include/my.h"

char	*subs_cond(char*, char*);
char	*add_cond(char*, char*);
char	*chang_sign(char*);
int	my_str_iszero(char const *);
char	*clear_result(char*);

char	*malloc_res(int i, int j)
{
	char	*res;

	if (i < j)
		res = malloc(sizeof(char*) * (j + 1));
	else
		res = malloc(sizeof(char*) * (i + 1));
	return (res);
}

char	*infin_add(char *nb1, char *nb2)
{
	char	*result;

	if (my_str_iszero(nb1) == 1 && my_str_iszero(nb2) == 1)
		result = "0";
	else {
		if (nb1[0] == '-' && nb2[0] == '-'){
			result = add_cond(&nb1[1], &nb2[1]);
			result = chang_sign(result);
		} else if (nb1[0] != '-' && nb2[0] != '-'){
			result = add_cond(nb1, nb2);
		} else
			result = subs_cond(nb1, nb2);
	}
	result = clear_result(result);
	return (result);
}
