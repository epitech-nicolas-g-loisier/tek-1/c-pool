/*
** EPITECH PROJECT, 2017
** infin_mod
** File description:
** ininity of modulo
*/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/my.h"
#include "../../include/bistro.h"

char	*infin_mod(char *nb1, char *nb2)
{
	char	*result = malloc(sizeof(char) * my_strlen(nb1));

	result = infin_div(nb1, nb2);
	result = infin_add(nb1, chang_sign(mult_cond(result, nb2)));
	return (result);
}
