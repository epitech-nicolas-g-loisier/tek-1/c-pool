/*
** EPITECH PROJECT, 2017
** bistro-matic
** File description:
** make calcul for bistro
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "../../include/my.h"

int	check_is_neg(char);
int	check_op_is_prio(char);
int	check_op(char);
char	*clear_result(char*);
char	*infin_add(char*, char*);
char	*infin_div(char*, char*);
char	*infin_mod(char*, char*);
char	*chang_sign(char*);
char	*mult_cond(char*, char*);
char	*clear_mult_op(char*);
char	*original_basis(char*, int);
char	*convertbase(char*, int);

char	*new_calcul(char *result, char *calcul, int stop)
{
	int	i = 0;
	int	j = 0;
	int	res_len = my_strlen(result) - 1;
	char	*new_result = malloc(sizeof(char*) * (res_len + stop));

	while (i < stop){
		new_result[i] = calcul[i];
		i++;
	}
	while (result[j] != '\0'){
		new_result[i] = result[j];
		i++;
		j++;
	}
	while (calcul[i] != '\0'){
		new_result[i] = calcul[i];
		i++;
	}
	new_result[i] = 0;
	new_result = clear_mult_op(new_result);
	return (new_result);
}

char	*find_summand(char *src)
{
	int	i = 0;
	int	tmp = 0;
	char	*nb;

	if (src[i] == '-')
		i++;
	while (tmp == 0 && src[i] != '\0'){
		i++;
		if (check_op_is_prio(src[i]) == 1)
			tmp = 1;
		else if (src[i] == '+' || src[i] == '-')
			tmp = 1;
	}
	nb = malloc(sizeof(char*) * (i + 6000000));
	tmp = 0;
	while (tmp < i){
		nb[tmp] = src[tmp];
		tmp++;
	}
	nb[tmp] = 0;
	return (nb);
}

char	*full_result(char *result, long len)
{
	long	res_len = my_strlen(result) - 1;
	char	*new_result = malloc(sizeof(char*) * len);

	new_result[len] = 0;
	len--;
	if (result[0] == '-'){
		new_result[0] = '-';
		result[0] = '0';
	} else
		new_result[0] = '0';
	while (res_len >= 0){
		new_result[len] = result[res_len];
		res_len--;
		len--;
	}
	while (len > 0){
		new_result[len] = '0';
		len--;
	}
	return (new_result);
}

char	*last_calc(char *b_nb2, char *calc, int begin_nb1, int base)
{
	char	*nb1 = find_summand(&calc[begin_nb1]);
	char	*nb2 = find_summand(b_nb2);
	int	res_len = my_strlen(nb1) + my_strlen(nb2) + 1;
	char	ope = b_nb2[-1];
	char	*result = malloc(sizeof(char*) * (res_len));

	nb1 = convertbase(clear_result(nb1), base);
	nb2 = convertbase(clear_result(nb2), base);
	if (ope == '+')
		result = infin_add(nb1, nb2);
	else if (ope == '-'){
		nb2 = chang_sign(nb2);
		result = infin_add(nb1, nb2);
	}
	free(nb1);
	free(nb2);
	if (base != 10)
		result = original_basis(result, base);
	result = full_result(result, res_len);
	calc = new_calcul(result, calc, begin_nb1);
	free(result);
	return (calc);
}

char	*prio_calc(char *b_nb2, char *calc, int begin_nb1, int base)
{
	char	*nb1 = find_summand(&calc[begin_nb1]);
	char	*nb2 = find_summand(b_nb2);
	int	res_len = my_strlen(nb1) + my_strlen(nb2) + 1;
	char	ope = b_nb2[-1];
	char	*result = malloc(sizeof(char*) * res_len);

	nb1 = convertbase(clear_result(nb1), base);
	nb2 = convertbase(clear_result(nb2), base);
	if (ope == '*')
		result = mult_cond(nb1, nb2);
	else if (ope == '/')
		result = infin_div(nb1, nb2);
	else
		result = infin_mod(nb1, nb2);
	free(nb1);
	free(nb2);
	if (base != 10)
		result = original_basis(result, base);
	result = full_result(result, res_len);
	calc = new_calcul(result, calc, begin_nb1);
	free(result);
	return (calc);
}

char	*calcul(char *calc, int base)
{
	int	i = 0;
	int	nb1 = 0;
	int	nb2 = 0;

	while (check_op_is_prio(calc[i]) != 1 && calc[i] != '\0'){
		if (calc[i] == '-' && check_is_neg(calc[i]) == 1){
			i++;
			nb1 = i;
		}
		else if (calc[i] == '+' || calc[i] == '-'){
			i++;
			nb1 = i;
		}
		i++;
	}
	if (check_op_is_prio(calc[i]) == 1){
		nb2 = i + 1;
		calc = calcul(prio_calc(&calc[nb2], calc, nb1, base), base);
	}
	i = 0;
	nb1 = i;
	if (calc[i] == '-')
		i++;
	while (calc[i] != '+' && calc[i] != '-' && calc[i] != '\0'){
		i++;
	}
	if (calc[i] == '+' || calc[i] == '-'){
		nb2 = i + 1;
		calc = calcul(last_calc(&calc[nb2], calc, nb1, base), base);
	}
	return (calc);
}
