/*
** EPITECH PROJECT, 2017
** bistro-matic
** File description:
** infinity multiplication
*/

#include <stdlib.h>
#include <stdio.h>

int	my_strlen(char const *str);
char	*clear_nb(char *nb);
char	*clear_result(char *res);

char	*infin_mult(char *nb1, char *nb2, char *result)
{
	int	i = my_strlen(nb1);
	int	j = my_strlen(nb2);
	int	len = i + j;
	int	min_res;

	if (nb1[0] == '0' || nb2[0] == '0'){
		return ("0");
	}
	while (j > 0){
		i = my_strlen(nb1);
		len = i + j;
		while (i > 0){
			min_res = (nb1[i - 1] - 48) * (nb2[j - 1] - 48);
			result[len] += min_res % 10;
			if (result[len] > 57){
				result[len] -= 10;
				result[len - 1] += 1;
			}
			result[len - 1] += min_res / 10;
			if (result[len - 1] > 57){
				result[len - 1] -= 10;
				result[len - 2] += 1;
			}
			i--;
			len--;
		}
		j--;
	}
	return (clear_result(result));
}

char	*mult_cond(char *nb1, char *nb2)
{
	int	i = my_strlen(clear_nb(nb1));
	int	j = my_strlen(clear_nb(nb2));
	int	len = i + j;
	char	*result;

	if (nb1[0] == '-' && nb2[0] == '-'){
		result = malloc(sizeof(char*) * (len));
	}
	else if (nb1[0] == '-' || nb2[0] == '-'){
		result = malloc(sizeof(char*) * (len + 1));
		result[0] = '-';
	}
	else
		result = malloc(sizeof(char*) * (len));
	result[len + 1] = '\0';
	while (len > 0){
		result[len] = '0';
		len--;
	}
	if (result[0] != '-')
		result[0] = '0';
	return (infin_mult(clear_nb(nb1), clear_nb(nb2), result));
}
