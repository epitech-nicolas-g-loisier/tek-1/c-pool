/*
** EPITECH PROJECT, 2017
** Bistro-matic
** File description:
** infini division
*/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/my.h"

int	my_strlen(char const*);
char	*clear_result(char*);
int	my_strcmpbistro(char const*, char const*);
char	*chang_sign(char*);
char	*infin_add(char*, char*);
char	*clear_nb(char*);

char	*check_neg(char nb1, char *nb2, char *result, char *tmp_nb1)
{
	if (tmp_nb1[1] == 0){
		tmp_nb1[1] = '0';
		tmp_nb1[2] = 0;
	}
	tmp_nb1 = clear_result(tmp_nb1);
	if (nb1 == '-' || nb2[0] == '-'){
		if (nb2[0] == '-')
			nb2 = clear_nb(nb2);
		if (my_strcmpbistro(tmp_nb1, nb2) > 0){
			result = infin_add(result, "1");
		}
		result = chang_sign(result);
	}
	return (result);
}

char	*division(char *nb1, char *nb2)
{
	int	len1 = my_strlen(nb1);
	int	len2 = my_strlen(nb2);
	int	i = 0;
	int	j = 0;
	int	count = 0;
	int	decal = 1;
	char	*result = malloc(sizeof(char*) * (len1 - 1));
	char	*tmp_nb1 = malloc(sizeof(char*) * (len1));
	char	*tmp_nb2 = nb2;
	char	*nbr1 = nb1;

	result[0] = '0';
	if (nb1[0] == '-')
		nb1 = clear_result(chang_sign(nb1));
	if (nb2[0] != '-')
		tmp_nb2 = clear_result(chang_sign(nb2));
	else
		len2--;
	while (i < len1){
		count = 0;
		while (i <= len2){
			tmp_nb1[j] = nb1[i];
			i++;
			j++;
		}
		tmp_nb1[j] = 0;
		tmp_nb1 = clear_result(tmp_nb1);
		while (my_strcmpbistro(tmp_nb1, &tmp_nb2[1]) >= 0){
			tmp_nb1 = infin_add(tmp_nb1, tmp_nb2);
			count++;
		}
		if (count > 9){
			result[decal - 1] += (count / 10);
			count = count % 10;
		}
		result[decal] = count + 48;
		j = my_strlen(tmp_nb1);
		len2++;
	decal++;
	}
	if (count == 0)
		decal--;
	result[decal] = 0;
	result = check_neg(nbr1[0], nb2, result, tmp_nb1);
	return (result);
}

char	*infin_div(char* nb1, char* nb2)
{
	char	*nbr2 = clear_nb(nb2);
	char	*result;

	if (nbr2[0] == '0'){
		exit(0);
	}
	if (my_strcmpbistro(nb2, "1") == 0)
		return (nb1);
	if (my_strcmpbistro(nb1, nbr2) == 0)
		return ("1");
	else if (my_strcmpbistro(nb1, nbr2) == -1)
		return ("0");
	else {
		if (nb1[0] == '-' && nb2[0] == '-')
			result = division(&nb1[1], &nb2[1]);
		else
			result = division(nb1, nb2);
	}
	return (clear_result(result));
}
