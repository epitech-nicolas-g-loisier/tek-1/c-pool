/*
** EPITECH PROJECT, 2017
** restore_syntax
** File description:
** restore the original syntax of the number
*/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/my.h"

/*
**This function restore the original syntax of the number
**given on parameter
*/
char *restore_syntax(char *expr, char *base_num, char *base_op)
{
	int i = 0;
	char dec_num[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	char dec_op[] = "()+-*/%";
	int len_expr = my_strlen(expr);
	char *expr_convert = malloc(sizeof(char) * len_expr);

	while(expr[i] != '\0'){
		if(contain(expr[i], dec_num) >= 0){
			expr_convert[i] = base_num[contain(expr[i], dec_num)];
		}
		else if(contain(expr[i], dec_op) >= 0){
			expr_convert[i] = base_op[contain(expr[i], dec_op)];
		}
		else{
			expr_convert[i] = expr[i];
		}
		i++;
	}
	expr_convert[i] = 0;
	return (expr_convert);
}
