/*
** EPITECH PROJECT, 2017
** bistro-matic
** File description:
** clear number
*/

#include <stdlib.h>

int	my_strlen(char const*);

char	*clear_nb(char *nb)
{
	int	i = 0;
	int	len = 0;
	int	test = 0;
	char	*new_nb;

	while (nb[i] != '\0'){
		if (test == 0 && (nb[i] == '0' || nb[i] == '-'))
			i++;
		else if (nb[i] >= '0' && nb[i] <= '9'){
			len++;
			i++;
			test = 1;
		}
	}
	new_nb = malloc(sizeof(char*) * (len + 1));
	new_nb[len + 1] = '\0';
	while (len >= 0){
		new_nb[len] = nb[i];
		len--;
		i--;
	}
	if (new_nb[0] == '\0')
		new_nb[0] = '0';
	return (new_nb);
}

char   *clear_result(char *res)
{
	int	i = my_strlen(clear_nb(res));
	char	*new_res;
	char	*clear_res = clear_nb(res);

	if (res[0] == '-'){
		new_res = malloc(sizeof(char*) * (i + 2));
		new_res[0] = '-';
		new_res[i + 2] = '\0';
		while (i >= 0){
			new_res[i + 1] = clear_res[i];
			i--;
		}
	}
	else {
		new_res = malloc(sizeof(char*) * (i + 1));
		new_res[i + 1] = '\0';
		while (i >= 0){
			new_res[i] = clear_res[i];
			i--;
		}
	}
	return (new_res);
}

char	*clear_neg(char *src)
{
	int	i = 0;
	int	len = my_strlen(src);
	int	count_neg = 0;
	int	count_pos = 0;
	char	*dest = malloc(sizeof(char*) * len);

	while (src[i] != 0){
		if (src[i] == '-')
			count_neg++;
		else if (src[i] == '+')
			count_pos++;
		i++;
	}
	i = 0;
	if ((count_neg % 2) == 1){
		dest[i] = '-';
		i++;
		count_neg--;
	} while (i < (count_neg + count_pos)){
		dest[i] = '0';
		i++;
	} while (src[i] != 0){
		dest[i] = src[i];
		i++;
	}
	dest[i] = 0;
	return (dest);
}

char	*clear_mult_op(char *src)
{
	int	i = 0;
	int	j = 0;
	int	count_neg = 0;
	char	*dest = malloc(sizeof(char*) * (my_strlen(src) + 1));

	
	while (src[i] != '\0'){
		while (src[i] == '-' || src[i] == '+'){
			dest[i] = '0';
			if (src[i] == '-')
				count_neg++;
			i++;
		}
		if ((count_neg % 2) == 1)
			dest[j] = '-';
		else if (src[j] == '+')
			dest[j] = '+';
		count_neg = 0;
		dest[i] = src[i];
		i++;
		j = i;
	}
	dest[i] = 0;
	return (dest);
}
