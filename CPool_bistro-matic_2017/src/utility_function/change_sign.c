/*
** EPITECH PROJECT, 2017
** Bistro-matic
** File description:
** change the sign of an number
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "../../include/my.h"

char	*chang_sign(char *nbr)
{
	long	i = 0;
	long	j = 0;
	char	*new_nbr = malloc(sizeof(char*) * (my_strlen(nbr) + 1));

	if (nbr[0] == '-'){
		new_nbr[i] = '0';
		j++;
	}
	else
		new_nbr[i] = '-';
	i++;
	while (nbr[j] != 0){
		new_nbr[i] = nbr[j];
		i++;
		j++;
	}
	new_nbr[i] = 0;
	return (new_nbr);
}
