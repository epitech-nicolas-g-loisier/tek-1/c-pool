/*
** EPITECH PROJECT, 2017
** bases
** File description:
** check the basis of a expression
*/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/macro.h"
#include "../../include/my.h"

/*
**This function check the base of a number given in parameter
**and return the int base of this number
*/

int	check_base(char *base_num, char *base_op)
{
	int	base_len = my_strlen(base_num);

	if (base_len < 2 && base_len > 62 || my_strlen(base_op) != 7){
		my_putstr(BASE_ERROR_MSG);
		return (BASE_ERROR);
	}
}

/*
**This function convert the a number given in parameter
**and return it in a decimal base (base 10)
*/

char	*convert_base(char *expr, char *base_num, char *base_op)
{
	int	i = 0;
	char	dec_num[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	char	dec_op[] = "()+-*/%";
	int	len_expr = my_strlen(expr);
	char	*expr_convert = malloc(sizeof(char) * len_expr);

	while (expr[i] != '\0'){
		if (contain(expr[i], base_num) >= 0){
			expr_convert[i] = dec_num[contain(expr[i], base_num)];
		} else if (contain(expr[i], base_op) >= 0){
			expr_convert[i] = dec_op[contain(expr[i], base_op)];
		} else {
			if (expr[i] == '\n'){
				expr_convert[i] = expr[i];
			} else{
				my_putstr(SYNTAX_ERROR_MSG);
				exit(ERROR);
			}
		}
		i++;
	}
	return (expr_convert);
}
