/*
** EPITECH PROJECT, 2017
** conversion
** File description:
** convert a decimal number in his original basis
*/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/my.h"
#include "../../include/bistro.h"

/*
**This function find the maximum of power
** to substract to the expression
*/
int find_maxpower(char *expr, int base)
{
	int i = 0;
	char *res = my_compute_power_string(base, i);
	while(my_strcmpbistro(my_compute_power_string(base, i + 1), expr) <= 0){
		i++;
		res = my_compute_power_string(base, i);
	}
	return (i);
}

/*
**This function search the character corresponding to the number take in parameter
*/
char find_char(char *num)
{
	int i = 0;
	char tab[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	int number = my_getnbr(num) - 10;
	while(number != i){
		i++;
	}
	return(tab[i]);
}

/*
**This function convert a decimal number in the orginal basis
**of the number take in parameter
*/
char *original_basis(char *expr, int base)
{
	int i = 0;
	char *res = malloc(sizeof(char) * my_strlen(expr) * 150);
	int p = find_maxpower(expr, base);
	char *nine = "9";
	char *power;
	char *divres;

	while(p >= 0){
		power = my_compute_power_string(base, p);
		divres = infin_div(expr, power);
		expr = infin_add(expr, chang_sign(mult_cond(power, divres)));
		if(my_strcmpbistro(divres, nine) >= 0){
			res[i] = find_char(divres);
		}
	        else{
			if(p >= 0)
				res[i] = divres[0];
		}
		i++;
		p--;
	}
	res[i] = '\0';
	return (res);
}
