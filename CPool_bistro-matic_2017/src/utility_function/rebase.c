/*
** EPITECH PROJECT, 2017
** rebase
** File description:
** convert bases into decimal numbers
*/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/my.h"
#include "../../include/macro.h"
#include "../../include/bistro.h"

char *my_compute_power_string(int nb, int p);

/*
**This function check if the digit > 9 and edit a string contain the integer value of this digit
** for example : A = 10, B = 11, C = 12
*/
char *check_digit(char c, char *digit)
{
	int dig = 0;
	if(my_isnum(c) == 1){
		digit[0] = c;
		digit[1] = '\0';
	}
	if(my_isupper(c) == 1){
		dig = c - 55;
		digit = get_string_nbr(dig);
	}
	if(my_islower(c) == 1){
		dig = c - 61;
		digit = get_string_nbr(dig);
	}
	return (digit);
}

/*
**This function convert an expression of a base B into a decimal base (10)
**and return the decimal number in form of string
*/
char *convertbase(char *expr, int base)
{
	int len = my_strlen(expr);
	int power = 0;
	int i = 0;
	char *res = malloc(len * len);
	char *digit = malloc(3);
	
	if (base == 10)
		return (expr);
	res = "0";
	while(len > 0){
		digit = check_digit(expr[i], digit);
		if(my_getnbr(digit) > (base - 1)){
			my_putstr(BASE_ERROR_MSG);
			exit (BASE_ERROR);
		}
		res = infin_add(res, mult_cond(digit, my_compute_power_string(base, len-1)));
		len--;
		i++;
	}
	free(digit);
	return (res);
}
