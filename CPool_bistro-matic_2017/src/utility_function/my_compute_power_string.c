/*
** EPITECH PROJECT, 2017
** my_compute_power_string
** File description:
** compute a power of two numbers and return to string
*/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/my.h"
#include "../../include/bistro.h"

/*
**This function compute the power of an int nb and a int power
**and return the result in a string
*/

char	*my_compute_power_string(int nb, int p)
{
	char	*nbs = get_string_nbr(nb);
	char	*ps = get_string_nbr(p);
	char	*res = malloc(sizeof(char) * 1000);

	res = nbs;
	if (p < 0)
		return ("0");
	else if (p == 0)
		return ("1");
	else
		if (p > 1){
			while (p > 1){
				res = mult_cond(res, nbs);
				p--;
			}
	}
	return (res);
}
