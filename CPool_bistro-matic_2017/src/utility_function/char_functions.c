/*
** EPITECH PROJECT, 2017
** char_functions
** File description:
** funcions that manage characters
*/

/*
**This function get the value of a character given in parameter
**and return the int value (ascii) of this number
*/
int getint(char c)
{
	int res = 0;
	
	if(c >= 48 && c <= 57){
		res = c - 48;
	}
	return (res);
}

/*
**Check if the character is an uppercase
*/
int my_isupper(char c)
{
	if(c >= 65 && c <= 90)
		return (1);
	else
		return (0);
}

/*
**Check if the character is a lowercase
*/
int my_islower(char c)
{
	if(c >= 97 && c <= 122)
		return (1);
	else
		return (0);
}

/*
**This function search a character in a string
**and return his position in the string or -1 if he is not present
*/
int contain(char c, char *str)
{
	int i = 0;

	while(str[i] != '\0'){
		if(c == str[i])
			return (i);
		else
			i++;
	}
	return (-1);
}
