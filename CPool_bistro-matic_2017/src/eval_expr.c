/*
** EPITECH PROJECT, 2017
** Bistro-matic
** File description:
** Evalexpr for string
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "../include/my.h"

char	*clear_result(char*);
char	*calcul(char*, int);
char    *clear_mult_op(char*);

char	*replace_par(char *src)
{
	int	str_len = my_strlen(src);
	int	i = 0;
	int	j = 1;
	char	*dest = malloc(sizeof(char*) * (str_len + 1));

	if (src[j] == '-'){
		dest[i] = '-';
		src[j] = '0';
		i++;
		j++;
	}
	dest[i] = '0';
	i++;
	dest[i] = '0';
	i++;
	while (i < str_len ){
		dest[i] = src[j];
		i++;
		j++;
	}
	dest[i] = 0;
	return (dest);
}

char	*find_calc_in_par(char *calc, int stop)
{
	int	i = 0;
	char	*calc_in_par = malloc(sizeof(char*) * (stop + 2));
	char	*calc_to_do = malloc(sizeof(char*) * (stop + 2));

	while (i <= stop){
		calc_in_par[i] = calc[i];
		i++;
	}
	calc_in_par[i] = 0;
	calc_to_do = replace_par(calc_in_par);
	free(calc_in_par);
	return (calc_to_do);
}

char	*replace_calc(char *par, int stop, char *src, int base)
{
	long	i = 0;
	long	begin = (my_strlen(src) - my_strlen(par));
	long	j = 0;
	char	*dest;
	char	*calc = calcul(find_calc_in_par(par, stop), base);

	dest = malloc(sizeof(char*) * (my_strlen(src) + 1));
	while (i < begin){
		dest[i] = src[i];
		i++;
	} while (j <= stop){
		dest[i] = calc[j];
		i++;
		j++;
	} while (src[i] != '\0'){
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

char	*parenthesis(char *src, int base)
{
	long	i = 0;
	long	j;

	while (src[i] != 0){
		if (src[i] == '(')
			j = i;
		else if (src[i] == ')'){
			return (replace_calc(&src[j], i - j, src, base));
		}
		i++;
	}
	return (src);
}

char	*eval_expr(char *src, int base)
{
	long	i = 0;

	src = clear_mult_op(src);
	while (src[i] != 0){
		if (src[i] == '('){
			src = parenthesis(src, base);
			i = -1;
		}
		i++;
	}
	src = calcul(src, base);
	src = clear_result(src);
	return (src);
}
