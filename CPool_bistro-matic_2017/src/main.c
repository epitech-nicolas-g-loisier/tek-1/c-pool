/*
** EPITECH PROJECT, 2017
** main
** File description:
** main of bistro project
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "../include/my.h"
#include "../include/macro.h"
#include "../include/bistro.h"

char	*eval_expr(char*, int);

int	main(int ac, char **av)
{
	char	expr[EXPR_SIZE + 1];
	int	offset;
	int	len;
	char	*convert_expr;
	int	base;
	int	i;
	char	*final_result;

	offset = 0;
	while ((len = read(0, expr + offset, EXPR_SIZE - offset)) > 0){
		offset = offset + len;
	}
	expr[offset] = '\0';
	if (len < 0)
		return (ERROR);
	else if (check_base(av[1], av[2]) == 84)
		return (ERROR);
	convert_expr = convert_base(expr, av[1], av[2]);
	base = my_strlen(av[1]);
	i = contain('\n', convert_expr);
	if (i >= 0)
		convert_expr[i] = '\0';
	final_result = eval_expr(convert_expr, base);
	final_result = restore_syntax(final_result, av[1], av[2]);
	my_putstr(final_result);
	return (0);
}
