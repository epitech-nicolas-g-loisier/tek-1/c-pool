#include <criterion/criterion.h>
#include "../include/bistro.h"
#include "../include/my.h"

Test(infin_add, test_more_more)
{
	cr_expect(my_strcmpbistro(infin_add("2000000000", "2000000000"), "4000000000") == 0);
	cr_expect(my_strcmpbistro(infin_add("2", "2"), "4") == 0);
	cr_expect(my_strcmpbistro(infin_add("10000", "650"), "10650") == 0);
	cr_expect(my_strcmpbistro(infin_add("9", "9"), "18") == 0);
	cr_expect(my_strcmpbistro(infin_add("5", "5"), "10") == 0);
	cr_expect(my_strcmpbistro(infin_add("19", "11"), "30") == 0);
}

Test(infin_add, test_less_less)
{
	cr_expect(my_strcmpbistro(infin_add("-2000000000", "-2000000000"), "-4000000000") == 0);
	cr_expect(my_strcmpbistro(infin_add("-2", "-2"), "-4") == 0);
	cr_expect(my_strcmpbistro(infin_add("-87", "-6"), "-93") == 0);
	cr_expect(my_strcmpbistro(infin_add("-7", "-7"), "-14") == 0);
	cr_expect(my_strcmpbistro(infin_add("-19", "-11"), "-30") == 0);
	cr_expect(my_strcmpbistro(infin_add("-2387", "-2"), "-2389") == 0);
}

Test(infin_add, test_more_less)
{
	cr_expect(my_strcmpbistro(infin_add("19", "-9"), "10") == 0);
	cr_expect(my_strcmpbistro(infin_add("10", "-9"), "1") == 0);
	cr_expect(my_strcmpbistro(infin_add("10", "-10"), "0") == 0);
	cr_expect(my_strcmpbistro(infin_add("100", "-9"), "91") == 0);
	cr_expect(my_strcmpbistro(infin_add("100", "-98"), "2") == 0);
	cr_expect(my_strcmpbistro(infin_add("201", "-8"), "193") == 0);
}

Test(infin_add, test_less_more)
{
	cr_expect(my_strcmpbistro(infin_add("-19", "9"), "-10") == 0);
	cr_expect(my_strcmpbistro(infin_add("-10", "9"), "-1") == 0);
	cr_expect(my_strcmpbistro(infin_add("-100", "9"), "-91") == 0);
	cr_expect(my_strcmpbistro(infin_add("-100", "98"), "-2") == 0);
	cr_expect(my_strcmpbistro(infin_add("-200", "8"), "-192") == 0);
}

Test(infin_mult, test_more_less)
{
	cr_expect(my_strcmpbistro(mult_cond("2909", "-897"), "-2609373") == 0, "1");
	cr_expect(my_strcmpbistro(mult_cond("-43", "247864132"), "-10658157676") == 0, "2");
	cr_expect(my_strcmpbistro(mult_cond("346811414", "-39677"), "-13760436473278") == 0, "3");
	cr_expect(my_strcmpbistro(mult_cond("-799", "64365"), "-51427635") == 0, "4");
	cr_expect(my_strcmpbistro(mult_cond("22354678", "-777"), "-17369584806") == 0, "5");
	cr_expect(my_strcmpbistro(mult_cond("-14","0"), "0") == 0, "6");

}

Test(infin_mult, test_more_more)
{
	cr_expect(my_strcmpbistro(mult_cond("942340","415671143"), "391703544894620") == 0);
	cr_expect(my_strcmpbistro(mult_cond("1234567890","6666"), "8229629554740") == 0);
	cr_expect(my_strcmpbistro(mult_cond("76","455"), "34580") == 0);
	cr_expect(my_strcmpbistro(mult_cond("936","4670"), "4371120") == 0);
	cr_expect(my_strcmpbistro(mult_cond("666","999"), "665334") == 0);
	cr_expect(my_strcmpbistro(mult_cond("14","0"), "0") == 0);

}

Test(infin_mult, test_less_less)
{
        cr_expect(my_strcmpbistro(mult_cond("-934","-666"), "622044") == 0);
        cr_expect(my_strcmpbistro(mult_cond("-987654","-12"), "11851848") == 0);
        cr_expect(my_strcmpbistro(mult_cond("-512","-8765"), "4487680") == 0);
        cr_expect(my_strcmpbistro(mult_cond("-666","-9999"), "6659334") == 0);
	cr_expect(my_strcmpbistro(mult_cond("-14","-0"), "0") == 0);
}


Test(infin_div, test_less_less)
{
        cr_expect(my_strcmp(infin_div("-934","-666"), "1") == 0);
        cr_expect(my_strcmp(infin_div("-987654","-12"), "82304") == 0);
        cr_expect(my_strcmp(infin_div("-512","-8765"), "0") == 0);
        cr_expect(my_strcmp(infin_div("-666","-9999"), "0") == 0);
        cr_expect(my_strcmp(infin_div("-14","-0"), "ERROR") == 0);
}


Test(infin_div, test_less_more)
{
        cr_expect(my_strcmp(infin_div("-678","23"), "-29") == 0);
        cr_expect(my_strcmp(infin_div("965","-102"), "-9") == 0);
        cr_expect(my_strcmp(infin_div("-512","8765"), "0") == 0);
        cr_expect(my_strcmp(infin_div("9090","-6"), "-1515") == 0);
        //cr_expect(my_strcmp(infin_div("-25","0"), "ERROR") == 0);
}


Test(infin_div, test_more_more)
{
        cr_expect(my_strcmp(infin_div("934","666"), "1") == 0);
        cr_expect(my_strcmp(infin_div("987","54"), "18") == 0);
        cr_expect(my_strcmp(infin_div("5012","875"), "5") == 0);
        cr_expect(my_strcmp(infin_div("3","6"), "0") == 0);
        cr_expect(my_strcmp(infin_div("14","0"), "ERROR") == 0);
}

/*
Test(infin_modulo, test_more_less)
{
        cr_expect(my_strcmp(infin_modulo("435","-5"), "0") == 0);
        cr_expect(my_strcmp(infin_modulo("87","-54"), "-21") == 0);
        cr_expect(my_strcmp(infin_modulo("-12","4332"), "4320") == 0);
        cr_expect(my_strcmp(infin_modulo("-3","92"), "89") == 0);
        cr_expect(my_strcmp(infin_modulo("14","0"), "ERROR") == 0);
}

Test(infin_modulo, test_less_less)
{
        cr_expect(my_strcmp(infin_modulo("-56","-666"), "-56") == 0);
        cr_expect(my_strcmp(infin_modulo("-67","-13220"), "-4") == 0);
        cr_expect(my_strcmp(infin_modulo("-5892","-761"), "-565") == 0);
        cr_expect(my_strcmp(infin_modulo("-4120441","-6"), "-1") == 0);
        cr_expect(my_strcmp(infin_modulo("14","0"), "ERROR") == 0);
}

Test(infin_modulo, test_more_more)
{
        cr_expect(my_strcmp(infin_modulo("934","666"), "268") == 0);
        cr_expect(my_strcmp(infin_modulo("90","100123"), "90") == 0);
        cr_expect(my_strcmp(infin_modulo("3123","12"), "3") == 0);
        cr_expect(my_strcmp(infin_modulo("000013216","739"), "601") == 0);
        cr_expect(my_strcmp(infin_modulo("14","0"), "ERROR") == 0);
}
*/
