#include <stdio.h>
#include <stdlib.h>

int my_getnbr(char const *str);

/*
**This function count the number of digit present in an integer
*/
int count_nbdigits(int nbr)
{
	char *max = malloc(sizeof(char) * 10000);
	int i = 1;
	max[0] = '9';
	int nbdigits = my_getnbr(max);
	while(nbr > nbdigits){
		nbdigits = my_getnbr(max);
		max[i] = '9';
		i++;
	}
	return (i - 1);
}
