/*
** EPITECH PROJECT, 2017
** my
** File description:
** prototypes library
*/

void my_putchar(char c);
int my_isneg(int nb);
int my_isnum(char c);
int my_put_nbr(int nb);
int my_putstr(char const *str);
int my_strlen(char const *str);
int my_getnbr(char const *str);
int my_compute_power_rec(int nb, int power);
int my_strcmp(char const *s1, char const *s2);
int my_strcmpbistro(char const *s1, char const *s2);
int my_getnbr(char const *str);
int count_nbdigits(int nbr);
char *get_string_nbr(int nb);
int my_str_iszero(char const *);
