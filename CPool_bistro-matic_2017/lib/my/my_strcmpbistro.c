/*
** EPITECH PROJECT, 2017
** my_strcmp
** File description:
** compare two strings
*/
#include <stdio.h>

int my_strlen(char const *str);

int my_strcmpbistro(char const *s1, char const *s2)
{
	int i = 0;
	int same = 0;
	char len1 = my_strlen(s1);
	char len2 = my_strlen(s2);

	if(len1 > len2){
		return(1);
	}
	else if(len1 < len2){
		return(-1);
	}
	else{
		while(s1[i] != '\0' && s2[i] != '\0'){
			if(s1[i] == s2[i]){
				same++;
			}
			else if(s1[i] < s2[i]){
				
				return (-1);
			}
			else{
				s1[i] > s2[i];		
					return (1);
			}
			i++;
		}
	}
	return (0);
}
