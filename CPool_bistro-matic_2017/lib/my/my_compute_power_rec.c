/*
** EPITECH PROJECT, 2017
** my_compute_power_rec
** File description:
** compute power rec
*/

int my_compute_power_rec(int nb, int p)
{
	int result = nb;

	if(result > 2147483647 || p < 0)
		return (0);
	if(p == 0)
		return (1);
	if(p > 1){
		result = result * my_compute_power_rec(result, (p - 1));
		return (result);
	}
}
