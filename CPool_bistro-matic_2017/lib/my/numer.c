/*
** EPITECH PROJECT, 2017
** number
** File description:
** convert the begining of a string into a interger
*/

#include <stdio.h>

int my_compute_power_rec(int nb, int p);

int my_strtol(char *str, char **endptr)
{
	int i = count_shift(str);
	*endptr += i;
	printf("i : %d\n", i);
	return (0);
}

int isnum(char c){
	if(c >= 48 && c <= 57)
		return (1);
	else
		return (0);
}

int count_shift(char *str){
	int i = 0;
	while(isnum(str[i]) == 0){
		i++;
	}
	while(isnum(str[i]) != 0){
		i++;
	}
	return (i);
}

int count_num(char *str){
	int i = 0;
	int nbchar = 0;
	while(isnum(str[i]) == 0){
		i++;
	}
	while(isnum(str[i]) != 0){
		i++;
		nbchar++;
	}
	return (nbchar);
}

int number(char **str)
{
	int i = 0;
	int final_number = 0;
	int nbchar = count_num(str[0]);
	while(isnum(str[0][i]) == 0){
		i++;
	}
	while(isnum(str[0][i]) == 1){
		printf("num : %c\n", str[0][i]);
		printf("nbnum : %d\n", nbchar);
		final_number = final_number + (str[0][i] - 48) * my_compute_power_rec(10, nbchar - 1);
		i++;
		nbchar--;
	}
	printf("final_number : %d\n", final_number);
	
	return (final_number);
}
