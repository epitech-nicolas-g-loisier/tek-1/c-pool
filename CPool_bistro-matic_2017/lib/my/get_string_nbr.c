/*
** EPITECH PROJECT, 2017
** my_compute_power_string
** File description:
** compute a power of two numbers and return to string
*/

#include <stdio.h>
#include <stdlib.h>

int count_nbdigits(int nbr);

/*
**This funtcion get a number in an int and return it in a string
*/
char *get_string_nbr(int nb)
{
	int lennb = count_nbdigits(nb);
	int cplennb = lennb - 1;
	int sauv = 0;
	char *res = malloc(lennb * sizeof(char));
	int div = 1;
	int i = 0;
	if(nb < 0){
		nb = nb * (-1);
		res[0] = '-';
		i++;
	}
	while(cplennb > 0){
		div = div * 10;
		cplennb--;
	}
	while(lennb >= 0){
		if(div < 10){
			res[i] = nb + 48;
			return (res);
		}
		res[i] = nb / div % div + 48;
		sauv = (nb / div % div) * div;
		nb = nb - sauv;
		div = div / 10;
		lennb--;
		i++;
	}
}
