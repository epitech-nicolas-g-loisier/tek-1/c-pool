/*
** EPITECH PROJECT, 2017
** my_strlen
** File description:
** lenght of a string
*/

int	my_strlen(char const *str)
{
	int nb = 0;

	while(str[nb] != '\0'){
		nb++;
	}
	return (nb);
}
