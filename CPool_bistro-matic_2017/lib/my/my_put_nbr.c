/*
** EPITECH PROJECT, 2017
** my_print_nbr
** File description:
** print number
*/

void my_putchar(char c);

int my_put_nbr(int nb)
{
	int c;
	int div = 1;
	int cpnb;

	if(nb < 0){
		nb = nb* (-1);
		my_putchar('-');
	}
	cpnb = nb;
	while(nb > 9){
		nb = nb / 10;
		div = div * 10;
	}
	while(div != 0){
		c = cpnb / div;
		cpnb = cpnb - c * div;
		div = div / 10;
		my_putchar(c + 48);
	}
	my_putchar(10);
}
